<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_images".
 *
 * @property integer $id
 * @property string $image
 * @property string $text
 * @property integer $is_visible
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['actualization'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'actualization' => 'Актулизация'
        ];
    }
}
