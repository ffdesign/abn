<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_news".
 *
 * @property integer $id
 * @property integer $id_category
 * @property integer $id_user
 * @property string $title
 * @property string $path
 * @property string $image
 * @property string $author_image
 * @property string $preview
 * @property string $text
 * @property string $tags
 * @property integer $position
 * @property integer $is_hot
 * @property integer $is_top
 * @property integer $is_visible
 * @property string $created
 * @property string $updated
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property TblNewsCategories $idCategory
 * @property TblUsers $idUser
 */
class News extends \yii\db\ActiveRecord
{

    public $searchontitle;
    public $updateDate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'id_user', 'title', 'preview', 'text'], 'required'],
            [['id_category', 'id_user', 'position', 'is_hot', 'is_top', 'is_visible', 'postponed'], 'integer'],
            [['preview', 'text'], 'string'],
            [['created', 'updated', 'postponed'], 'safe'],
            [['title', 'path', 'tags', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Категория',
            'id_user' => 'Пользователь',
            'title' => 'Заголовок',
            'path' => 'Путь',
            'preview' => 'Аннотация',
            'text' => 'Текст',
            'tags' => 'Теги',
            'position' => 'Позиция',
            'is_hot' => 'Горячая новость',
            'is_top' => 'В топе',
            'is_visible' => 'Видимость',
            'created' => 'Добавлена',
            'updated' => 'Обновлена',
            'meta_title' => 'МЕТА: Заголовок',
            'meta_description' => 'МЕТА: Описание',
            'meta_keywords' => 'МЕТА: Ключевые слова',
            '_deleteimage' => 'Удалить изображение',
            'searchontitle' => 'Искать только в заголовках',
            'updateDate' => 'Изменить дату',
            'postponed' => 'Отложенная публикация',
        ];
    }

    public function getTopNews($place) 
    {
        $model = $this->find()->where('is_visible=1 AND is_top='.$place)->orderBy('created desc')->one();
        if($model)
            return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategories::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    public function getImages()
    {
        return $this->hasMany(Images::className(), ['id_news' => 'id']);
    }

    public function getOneImage()
    {
        return $this->hasOne(Images::className(), ['id_news' => 'id']);
    }
}
