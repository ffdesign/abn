<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_subscribe_data".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 */
class SubscribeData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_subscribe_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['count_news', 'count_sends'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название рассылки',
        ];
    }
}
