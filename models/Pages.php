<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $text
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $is_visible
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'title', 'path'], 'required'],
            [['text'], 'string'],
            [['is_visible'], 'integer'],
            [['title', 'path', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'path' => 'Путь',
            'text' => 'Текст',
            'meta_title' => 'МЕТА: Заголовок',
            'meta_description' => 'МЕТА: Описание',
            'meta_keywords' => 'МЕТА: Ключевые слова',
            'is_visible' => 'Видимость',
        ];
    }
}
