<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_banners".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $url
 * @property integer $position
 * @property integer $is_visible
 */
class Banners extends \yii\db\ActiveRecord
{

    public $_image;
    public $_deleteimage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['id', 'position', 'is_visible'], 'integer'],
            [['title', 'image', 'url'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            '_image' => 'Изображение',
            'url' => 'Ссылка',
            'position' => 'Позиция',
            'place' => 'Расположение',
            'is_visible' => 'Видимость',
            '_deleteimage' => 'Удалить изображение',
        ];
    }
}
