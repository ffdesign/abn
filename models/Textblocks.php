<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_textblocks".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $is_visible
 */
class Textblocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_textblocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['is_visible'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'is_visible' => 'Видимость',
        ];
    }
}
