<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_news_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $color
 * @property integer $position
 * @property integer $is_visible
 * @property integer $is_home
 *
 * @property TblNews[] $tblNews
 */
class NewsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_news_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'is_visible', 'is_home'], 'integer'],
            [['title', 'path', 'color'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'path' => 'Путь',
            'color' => 'Цвет',
            'position' => 'Позиция',
            'is_visible' => 'Видимость',
            'is_home' => 'Показывать на главной',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['id_category' => 'id'])->where("is_visible = 1")->orderBy("created desc")->limit(4);
    }

    public function getArchive()
    {
        if(empty($_GET['date'])) {
            $date = time();
        } else {
            $date = $_GET['date'];
        }

        return $this->hasMany(News::className(), ['id_category' => 'id'])->where(["FROM_UNIXTIME(created, '%d-%m-%Y')" => $date, 'is_visible' => 1])->orderBy("created desc")->limit(4);
    }

    public function getOldNews()
    {
        $find = News::find()->where(["FROM_UNIXTIME(created, '%d-%m-%Y')" => $_GET['date'], 'is_visible' => 1])->orderBy("created desc")->limit(4)->all();
        if(isset($find)) {
            $date = time();
        } else {
            $date = strtotime($_GET['date']);
        }

        return $this->hasMany(News::className(), ['id_category' => 'id'])->where("created < '$date' OR 'is_visible' = 1")->orderBy("created desc")->limit(4);
    }
}
