<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_images".
 *
 * @property integer $id
 * @property string $image
 * @property string $text
 * @property integer $is_visible
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['is_visible'], 'integer'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'text' => 'Описание',
            'is_visible' => 'Видимость',
        ];
    }
}
