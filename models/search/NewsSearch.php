<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\models\News`.
 */
class NewsSearch extends News
{
    public function rules()
    {
        return [
            [['id', 'id_category', 'id_user', 'position', 'is_hot', 'is_top', 'is_visible'], 'integer'],
            [['title', 'path', 'preview', 'text', 'tags', 'created', 'updated', 'meta_title', 'meta_description', 'meta_keywords', 'searchontitle'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        if(isset($_GET['id']))
            $query = News::find()->where("id_category = " . $params['id']);
        else
            $query = News::find();


        $query->orderBy('created DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_category' => $this->id_category,
            'id_user' => $this->id_user,
            'position' => $this->position,
            'is_hot' => $this->is_hot,
            'is_top' => $this->is_top,
            'is_visible' => $this->is_visible,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);


        if($this->searchontitle)
            $query->andFilterWhere(['like', 'title', $this->text]);
        else
            $query->andFilterWhere(['like', 'text', $this->text]);
        

        return $dataProvider;
    }
}
