<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NewsCategories;

/**
 * NewsCategoriesSearch represents the model behind the search form about `app\models\NewsCategories`.
 */
class NewsCategoriesSearch extends NewsCategories
{
    public function rules()
    {
        return [
            [['id', 'position', 'is_visible', 'is_home'], 'integer'],
            [['title', 'path', 'color'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = NewsCategories::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
            'is_visible' => $this->is_visible,
            'is_home' => $this->is_home,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'color', $this->color]);

        return $dataProvider;
    }
}
