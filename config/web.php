<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
    'components' => [
        'cache' => [
            'class' => 'yii\caching\DbCache',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        // 'mail' => [
        //     'class' => 'yii\swiftmailer\Mailer',
        //     // 'useFileTransport' => true,
        //     'transport' => [
        //          'class' => 'Swift_SmtpTransport',
        //          'host' => 'smtp.yandex.ru',
        //          'username' => 'noreply@foxunion.ru',
        //          'password' => 'jfFDk31Skfa21_',
        //          'port' => '465',
        //          'encryption' => 'ssl',
        //      ],
        // ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'cookieValidationKey' => 'LtZwZbAL1Z0syBA-QaGdbP0xHQToujhn',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:(news)>/<action:all>' => 'news/all', 
                '<controller:(news)>/<action:page>' => 'news/page', 
                '<controller:(news)>/<action:archive>/<date>' => 'news/archive', 
                '<controller:(news)>/category/<category>' => 'news-categories/view', 
                '<controller:(news)>/page/<category>' => 'news-categories/page', 
                '<controller:(pages|news)>/<action:(index|create)>' => '<controller>/<action>',
                '<controller:(pages|news)>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller:(pages|news)>/<id:\d+>' => '<controller>/view',
                '<controller:(pages|news)>/<path>' => '<controller>/view',
                '<controller:(news)>/<category>/<path>' => '<controller>/view',
                '<controller:(pages|news)>/<action>/<path:\w+>' => '<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller:(news-categories)>/<path>' => '<controller>/view',
                'search' => '/site/search',
                'unsubscribe' => '/subscribe/unsubscribe',
                'contacts' => '/site/contact',
                'rss' => '/site/rss',
                'rss-rambler' => '/site/rss-rambler',
                
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

// if (YII_ENV_DEV) {
//     // configuration adjustments for 'dev' environment
//     $config['bootstrap'][] = 'debug';
//     $config['modules']['debug'] = 'yii\debug\Module';
//     $config['modules']['gii'] = 'yii\gii\Module';
// }

return $config;
