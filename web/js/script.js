//маленький хак, чтоб работал live
jQuery.fn.live = function (types, data, fn) {
    jQuery(this.context).on(types,this.selector,data,fn);
    return this;
};

var i = 0;

$('#add-input-image').on('click', function() {
    i++;
    $('.input-image').append('<div id="image'+ i +'"><input type="hidden" name="News[image_true][]" value="0"><br><b>Изображение</b> <a class="removeNews"  id="image'+ i +'" href="#"><span class="glyphicon glyphicon-trash"></span></a> <br/> <input type="file" class="news-image-file"  id="news-image'+ i +'" name="News[image][]"> <label for="image-author'+ i +'">Автор изображения</label><input type="text" id="image-author'+ i +'" class="form-control" name="News[image_authorc][]"><p></p> <label for="news-image-preview'+ i +'">Описание</label> <br/> <input type="text" id="news-image-preview'+ i +'" class="form-control" name="News[image_previewc][]"></div>');
    console.log(i);
    return false;
});

$('a.removeNews').live('click', function() {
	var id = $(this).attr('id');
	console.log(id);
	$('#'+ id).remove();
	return false;
});

$('#searchOnTitle').live('click', function() {
    $('#bigsearch').submit();
    return false;
});


$('.filterCat').live('click', function() {
    tfis = $(this);
    idcategory = tfis.data().idcategory;

    if(tfis.hasClass('act')) {
        tfis.removeClass('act');
        $('.cat'+idcategory).hide();
    } else {
        tfis.addClass('act');
        $('.cat'+idcategory).show();
    }

    return false;
});

$('#news-created').attr('disabled', true);
$('#updateDate').live('click', function() {
    if($('#news-created').hasClass('active')) {
        $('#news-created').attr('disabled', true);
        $('#news-created').removeClass('active');
    } else {
        $('#news-created').attr('disabled', false);
        $('#news-created').addClass('active');
    }
});

$('#showEmail').live('click', function() {
    if($('.list_subscribe_email').hasClass('hidden'))
        $('.list_subscribe_email').removeClass('hidden');
    else
        $('.list_subscribe_email').addClass('hidden');
});

$("#modal-launcher, #modal-background, #modal-close").live('click', function () {
        $("#modal-content,#modal-background").toggleClass("active");
    });

$('a#loadNews').live('click', function() {
    var tfis = $(this), current_page = tfis.attr('alt');
    $.ajax({
        url: '/news/page/' + tfis.data().categoryid + '?page=' + current_page,
        type: 'POST',
        data: {'current_page' : current_page},
        dataType: 'html',
        beforeSend: function(){
            $('.loadNews').append('<center><img id="preloader" src="/images/ajax-loader.gif"></center>');
        },
        success: function(data){
            if(data) {
                $('#preloader').remove();
                $('.loadNews').append(data);
                tfis.attr('alt', parseInt(current_page) + 1);
            } else {
                $('#preloader').remove();
                $('#loadNews').remove();
                $('.razdel_more_link').append('<span id="endPageMsg">Все новости загружены</span>');
                $("#endPageMsg").fadeOut(2000)
            }
        },
    });

    return false;
});

$('a#loadNewsAll').live('click', function() {
    var tfis = $(this), current_page = tfis.attr('alt');
    $.ajax({
        url: '/news/page?page=' + current_page,
        type: 'POST',
        data: {'current_page' : current_page},
        dataType: 'html',
        beforeSend: function(){
            $('.loadNewsAll').append('<center><img id="preloader" src="/images/ajax-loader.gif"></center>');
        },
        success: function(data){
            if(data) {
                $('#preloader').remove();
                $('.loadNewsAll').append(data);
                tfis.attr('alt', parseInt(current_page) + 1);
            } else {
                $('#preloader').remove();
                $('#loadNewsAll').remove();
                $('.razdel_more_link').append('<span id="endPageMsg">Все новости загружены</span>');
                $("#endPageMsg").fadeOut(2000)
                
            }
        },
    });

    return false;
});

$('a#loadNewsSearch').live('click', function() {
    var tfis = $(this), current_page = tfis.attr('alt');
    $.ajax({
        url: '/site/page/?page=' + current_page + '&search=' + $('#msearch').val(),
        type: 'POST',
        data: {'current_page' : current_page},
        dataType: 'html',
        beforeSend: function(){
            $('.loadNewsSearch').append('<center><img id="preloader" src="/images/ajax-loader.gif"></center>');
        },
        success: function(data){
            if(data) {
                $('#preloader').remove();
                $('.loadNewsSearch').append(data);
                tfis.attr('alt', parseInt(current_page) + 1);
            } else {
                $('#preloader').remove();
                $('#loadNewsSearch').remove();
                $('.razdel_more_link').append('<span id="endPageMsg">Все новости загружены</span>');
                $("#endPageMsg").fadeOut(2000)
                
            }
        },
    });

    return false;
});


Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    odnoklassniki: function(purl, text) {
        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
        url += '&st.comments=' + encodeURIComponent(text);
        url += '&st._surl='    + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    mailru: function(purl, ptitle, pimg, text) {
        url  = 'http://connect.mail.ru/share?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&imageurl='    + encodeURIComponent(pimg);
        Share.popup(url)
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};

function addLink() {
        var body_element = document.getElementsByTagName('body')[0];
        var selection;
        selection = window.getSelection();
        var pagelink = " Читайте дальше: <a href='"+document.location.href+"'>"+document.location.href+"</a> © АБН"; // В этой строке поменяйте текст на свой
        var copytext = selection + pagelink;
        var newdiv = document.createElement('div');
        newdiv.style.position='absolute';
        newdiv.style.left='-99999px';
        body_element.appendChild(newdiv);
        newdiv.innerHTML = copytext;
        selection.selectAllChildren(newdiv);
        window.setTimeout(function() {
        body_element.removeChild(newdiv);
        },0);
    }
document.oncopy = addLink;

$('#datetimepicker1').datetimepicker({
    language: 'ru',
    'format': 'L H:mm',
});

$('#users-email').focus().select();
