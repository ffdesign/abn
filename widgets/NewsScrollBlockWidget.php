<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\News;
use app\helpers\TextHelper; 
use yii\db\Connection;

class NewsScrollBlockWidget extends Widget
{
    public function run()
    {
        $arDays = array();
        // $connection = new Connection;
        // $connection->beginCache(300);
        $date = '';
        if(isset(Yii::$app->request->get()['date'])) {
            $date = " AND created < '".strtotime($_GET['date'])."'";
        }
        
        if(News::find()->where('is_visible = 1'.$date)->limit('20')->orderBy('created desc')->all()) {
            $model = News::find()->where('is_visible = 1'.$date)->limit('20')->orderBy('created desc')->all();
        } else {
            $model = News::find()->where('is_visible = 1 AND created <' . time())->limit('20')->orderBy('created desc')->all();
        }
        // $connection->endCache();
        foreach ($model as $day): 
            $arDays[] = TextHelper::formatDate($day->created, 'm'); 
        endforeach; 

        return $this->render('NewsScrollBlock', [
                'model' => $model,
                'arDays' => array_unique($arDays)
            ]);
    }
}