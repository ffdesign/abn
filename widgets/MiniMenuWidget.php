<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\NewsCategories;


class MiniMenuWidget extends Widget
{
    public function run()
    {
        $modelNewsCategories = new NewsCategories;
        $category = $modelNewsCategories->find()->where(['is_visible' => 1])->limit(8)->all();

        return $this->render('miniMenu', [
            'category' => $category,
        ]);

    }

    
}
