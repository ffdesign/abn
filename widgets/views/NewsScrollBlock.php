<?
use app\helpers\TextHelper; 
?>
<section class="lenta_block">
    <div class="lenta_block_title">Все новости</div>
    <div class="lenta_block_scroll">
        <?// if ($this->beginCache('1', ['duration' => 300])): ?>
            <? foreach ($arDays as $key => $day): ?>
                <? if($key != 0): ?><div class="lenta_block_datatitle"><?= $day ?></div><? endif; ?>
                <? foreach ($model as $news): ?>
                    <? if($day == TextHelper::formatDate($news->created, 'm')): ?>
                        <div class="lenta_block_item <?= $news->is_hot ? 'hotlenta' : '' ?>">
                            <div class="post_data"><span style="padding-right: 0px;"><?= date('H:i', $news->created) ?></span> / <span class="upper"><a href="/news/category/<?=$news->category->path?>" class="razdel_link_simple"><?=$news->category->title?></a></span></div>
                            <a href="/news/<?=$news->category->path?>/<?=$news->path?>" class="lenta_block_item_link"><?=$news->title?></a>                    
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            <? endforeach; ?>
        <?// $this->endCache(); endif;?>
    </div>
    <div class="lenta_more_link"><a href="/news/all"><span>Все новости</span></a></div>
</section>