<?
use app\helpers\TextHelper; 
?>
</header><!-- #header-->

<section class="textpage_subheader">
<nav>
	 <ul class="mainmenu">
		<? foreach ($category as $row): ?>
            <li><a href="/news/category/<?=$row->path?>" class="menuitem1" style="border-top: 2px solid <?=$row->color?>"><?=$row->title?></a></li>
        <? endforeach; ?>
	</ul>
</nav>

 <section class="textpage_tophot cls">
 	<? if(isset($top1)): ?>
		<div class="textpage_hotlist_item <?= $top1->is_hot ? 'tophot' : '' ?> ">
			<? if($top1->oneImage):
		        $image = '/images/news/'.$top1->id.'/thumb/'.md5($top1->oneImage->image.$top1->id).'-115x76.png'; ?>
		     	<a href="/news/<?=$top1->category->path?>/<?=$top1->path?>" class="textpage_tophot_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
			<? endif ?>
			 <div class="topblock_hotlist_linkblock">
			     <a href="/news/<?=$top1->category->path?>/<?=$top1->path?>" class="textpage_topblock_hotlist_link"><?=TextHelper::character_limiter($top1->title, 80)?></a>
			     <div class="post_data"><span><?= TextHelper::formatDate($top1->created, 'mmt') ?></span><a href="/news/category/<?=$top1->category->path?>" class="razdel_link2" style="background-color: <?=$top1->category->color?>"><?=$top1->category->title?></a></div>
			 </div>
		</div>
	<? endif; ?>

	<? if(isset($top2)): ?>
		<div class="textpage_hotlist_item <?= $top2->is_hot ? 'tophot' : '' ?> ">
			<? if($top2->oneImage):
		        $image = '/images/news/'.$top2->id.'/thumb/'.md5($top2->oneImage->image.$top2->id).'-115x76.png'; ?>
		     	<a href="/news/<?=$top2->category->path?>/<?=$top2->path?>" class="textpage_tophot_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
			<? endif ?>
			 <div class="topblock_hotlist_linkblock">
			     <a href="/news/<?=$top2->category->path?>/<?=$top2->path?>" class="textpage_topblock_hotlist_link"><?=TextHelper::character_limiter($top2->title, 80)?></a>
			     <div class="post_data"><span><?= TextHelper::formatDate($top2->created, 'mmt') ?></span><a href="/news/category/<?=$top2->category->path?>" class="razdel_link2" style="background-color: <?=$top2->category->color?>"><?=$top2->category->title?></a></div>
			 </div>
		</div>
	<? endif; ?>

	<? if(isset($top3)): ?>
		<div class="textpage_hotlist_item <?= $top3->is_hot ? 'tophot' : '' ?> ">
			<? if($top3->oneImage):
		        $image = '/images/news/'.$top3->id.'/thumb/'.md5($top3->oneImage->image.$top3->id).'-115x76.png'; ?>
		     	<a href="/news/<?=$top3->category->path?>/<?=$top3->path?>" class="textpage_tophot_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
			<? endif ?>
			 <div class="topblock_hotlist_linkblock">
			     <a href="/news/<?=$top3->category->path?>/<?=$top3->path?>" class="textpage_topblock_hotlist_link"><?=TextHelper::character_limiter($top3->title, 80)?></a>
			     <div class="post_data"><span><?= TextHelper::formatDate($top3->created, 'mmt') ?></span><a href="/news/category/<?=$top3->category->path?>" class="razdel_link2" style="background-color: <?=$top3->category->color?>"><?=$top3->category->title?></a></div>
			 </div>
		</div>
	<? endif; ?>
 </section>		
</section>	