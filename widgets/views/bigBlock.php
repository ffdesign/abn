<?
use app\helpers\TextHelper; 
use app\helpers\YiiHelper; 
?>
<? $banner = YiiHelper::getBanners('Шапка');  ?>
<section class="bann1000_90">
<? if($banner): ?>
    <a target="_blank" href="<?=$banner['url']?>"><img src="/images/banners/<?=$banner['image']?>"></a>
<? endif; ?>
</section>
<nav>
    <ul class="mainmenu">
        <? foreach ($category as $row): ?>
            <li><a href="/news/category/<?=$row->path?>" class="menuitem1" style="border-top: 2px solid <?=$row->color?>"><?=$row->title?></a></li>
        <? endforeach; ?>
    </ul>
</nav>
</header><!-- #header-->
<section id="content_index" class="content_index_topblock">
<section id="index_offset_area" class="cls">
            <? if(isset($top1)): ?>
                <div class="content_index_topblock_area1">
                    <div class="topblock_tophot <?= $top1->is_hot ? 'tophot' : '' ?>">
                        <? if($top1->oneImage):
                        $image = '/images/news/'.$top1->id.'/thumb/'.md5($top1->oneImage->image.$top1->id).'-306x180.png'; ?>
                        <a href="/news/<?=$top1->category->path . '/' . $top1->path?>" class="topblock_hotlist_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                    <? endif; ?>
                        <a href="/news/<?=$top1->category->path . '/' . $top1->path?>" class="topblock_tophot_link"><?=$top1->title?></a>
                        <div class="topblock_tophot_text"><?= $top1->preview ?></div>
                        <div class="post_data"><span><?= TextHelper::formatDate($top1->created, 'mt') ?></span><a href="/news/category/<?=$top1->category->path?>" class="razdel_link1" style="background-color: <?=$top1->category->color?>"><?=$top1->category->title?></a></div>
                    </div>
                </div>
            <? endif; ?>
            <div class="content_index_topblock_area2">
            <? if(isset($top2)): ?>
                <div class="topblock_hotlist_item <?= $top2->is_hot ? 'tophot' : '' ?> cls">
                    <? if($top2->oneImage):
                    $image = '/images/news/'.$top2->id.'/thumb/'.md5($top2->oneImage->image.$top2->id).'-115x76.png'; ?>
                    <a href="/news/<?=$top2->category->path . '/' . $top2->path?>" class="topblock_hotlist_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                <? endif; ?>
                    <div class="topblock_hotlist_linkblock">
                        <a href="/news/<?=$top2->category->path . '/' . $top2->path?>" class="topblock_hotlist_link"><?=$top2->title?></a>
                        <div class="post_data"><span><?= TextHelper::formatDate($top2->created, 'mt') ?></span><a href="/news/category/<?=$top2->category->path?>" class="razdel_link2" style="background-color: <?=$top2->category->color?>"><?=$top2->category->title?></a></div>
                    </div>
                </div>
            <? endif; ?>
            <? if(isset($top3)): ?>
                <div class="topblock_hotlist_item <?= $top3->is_hot ? 'tophot' : '' ?> cls">
                <? if($top3->oneImage):
                    $image = '/images/news/'.$top3->id.'/thumb/'.md5($top3->oneImage->image.$top3->id).'-115x76.png'; ?>
                    <a href="/news/<?=$top3->category->path . '/' . $top3->path?>" class="topblock_hotlist_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                <? endif; ?>
                    <div class="topblock_hotlist_linkblock">
                        <a href="/news/<?=$top3->category->path . '/' . $top3->path?>" class="topblock_hotlist_link"><?=$top3->title?></a>
                        <div class="post_data"><span><?= TextHelper::formatDate($top3->created, 'mt') ?></span><a href="/news/category/<?=$top3->category->path?>" class="razdel_link4" style="background-color: <?=$top3->category->color?>"><?=$top3->category->title?></a></div>
                    </div>
                </div>
            <? endif; ?>
            <? if(isset($top4)): ?>
                <div class="topblock_hotlist_item <?= $top4->is_hot ? 'tophot' : '' ?> topblock_hotlist_item_last cls">
                    <? if($top4->oneImage):
                    $image = '/images/news/'.$top4->id.'/thumb/'.md5($top4->oneImage->image.$top4->id).'-115x76.png'; ?>
                    <a href="/news/<?=$top4->category->path . '/' . $top4->path?>" class="topblock_hotlist_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                <? endif; ?>
                    <div class="topblock_hotlist_linkblock">
                        <a href="/news/<?=$top4->category->path . '/' . $top4->path?>" class="topblock_hotlist_link"><?=$top4->title?></a>
                        <div class="post_data"><span><?= TextHelper::formatDate($top4->created, 'mt') ?></span><a href="/news/category/<?=$top4->category->path?>" class="razdel_link4" style="background-color: <?=$top4->category->color?>"><?=$top4->category->title?></a></div>
                    </div>
                </div>
            <? endif; ?>
        </div>
        <? $banner = YiiHelper::getBanners('Сквозной');  ?>
        <? if($banner): ?>
            <div class="content_index_topblock_area3"><a href="<?=$banner['url']?>" target="_blank"><img src="/images/banners/<?=$banner['image']?>" border="0" width="240" height="400"></a></div>
        <? endif; ?>
    
</section>