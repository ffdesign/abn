<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use Yii;
use yii\console\Controller;
use yii\caching\Cache;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {

    	$currency = array(
            'dollar' => 'R01235',
            'euro' => 'R01239'
        );

        $date = getdate();  // получаем ассоциативный массив с данными по дате

        $day_of_the_week = $date['wday']; // день недели

            //расчет коэффициента смещения
            switch ($day_of_the_week) {
                case 0:  $k1 = 2 ;  $k2 = 1;  break;   // воскресенье
                case 1:  $k1 = 3 ;  $k2 = 2;  break;   // понедельник
                case 2:  $k1 = 3 ;  $k2 = 0;  break;  // вторник
                default: $k1 = 1 ;  $k2 = 0;  break;   // среда, четверг, пятница, суббота
            }

        $month = $date['mon'] ;    // месяц
        $day = $date['mday'] ;     // число сегодня
        $yesterday = $day - $k1 ;  // число для получения курса на предыдущий день
        $today = $day - $k2 ;      // число для получения курса на сегодня
        $year = $date['year'] ;    // год


        $date_yesterday = date("d/m/Y", mktime(0, 0, 0, $month, $yesterday, $year )); // Генерация даты для курса предыдущего дня
        $date_today = date("d/m/Y", mktime(0, 0, 0, $month, $today, $year )); // Генерация даты для курса на сегодня

        $rate = array() ;

        foreach($currency as $key => $value) {
            $url = 'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1='.$date_yesterday.'&date_req2='.$date_today.'&VAL_NM_RQ='.$value ;
            $xml = simplexml_load_file($url);
            $rate_today = round(str_replace(',','.',$xml->Record[1]->Value), 2);
            $rate_yesterday = round(str_replace(',','.',$xml->Record[0]->Value), 2);
            $range = round($rate_today - $rate_yesterday, 2) ;

            if ($range > 0) {
                $range = '+' . $range;
                $img = 'up' ;
            } elseif ($range == 0) {
                $range = 0 ;
                $img = 0 ;
            } else {
                $img = 'down' ;
            }

            $rate[$key] = array(
             'price' => $rate_today,
             'change' => $range,
             'action' => $img
            );

        }


        $fp = fopen('web/rate.txt', 'w');
        foreach ($rate as $row):
			fwrite($fp, $row['price']. ';' . $row['action'] . ';');

        endforeach;
        fclose($fp);

        Yii::$app->getCache()->set('updateCurrency', true);

        // print_r($rate);
    }
}
