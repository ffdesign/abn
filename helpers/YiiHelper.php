<?php
namespace app\helpers;
use yii\web\Session;
use app\models\Users;
use app\models\Textblocks;
use app\models\Banners;
use app\models\News;

class YiiHelper
{
   static function is_auth() 
   {
        $session = new Session;
        if($session->has('auth'))
            return true;
   }

   static function getAuthName() 
   {
        $session = new Session;
        $model = new Users;
        $token = $session->get('auth');
        $userInfo = $model->find()->where("token = '$token'")->one();
        return $userInfo->email;
   }

   static function getAuthId() 
   {
        $session = new Session;
        $model = new Users;
        $token = $session->get('auth');
        $userInfo = $model->find()->where("token = '$token'")->one();
        return $userInfo->id;
   }

   static function getTextblock($title)
   {
        $model = new Textblocks;
        $query = $model->find()->where("title = '$title' && is_visible = 1")->one();
        if($query) {
            echo $query->text;
        } else {
            $model->title = $title;
            $model->save();
        }
   }

   static function getBanners($title)
   {
        $model = new Banners;
        $query = $model->find()->where(['place' => $title, 'is_visible' => 1])->orderBy('position desc')->one();
        if($query)
            return $query;
   }

   static function getLastUpdate() 
   {
    $model = News::find()->select('created')->where(['is_visible' => 1])->orderBy('created desc')->one();
    if($model)
        return $model;
   }

   static function getCurrency() 
   {
        $filename = "rate.txt";
        $handle = fopen($filename, "r");
        $rate = fread($handle, filesize($filename));
        fclose($handle);
        $rate = explode(';', $rate);

        return $rate;
   }
}
?>