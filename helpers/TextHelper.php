<?php
namespace app\helpers;

class TextHelper
{
    /**
   * Устанавливает лимит символов
   * @param string $str
   * @param integer $n
   * @param string $end_char
   * @return string 
   */
  static function character_limiter($str, $n = 500, $end_char = '&#8230;') {
      if (strlen($str) < $n) {
          return $str;
      }

      $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

      if (strlen($str) <= $n) {
          return $str;
      }

      $out = "";
      foreach (explode(' ', trim($str)) as $val) {
          $out .= $val . ' ';

          if (strlen($out) >= $n) {
              $out = trim($out);
              return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
          }
      }
  }

  /**
   * Экринирует строку
   *
   * @param  string  $string    экранируемая строка
   * @return string
   */
    static function escape($string) {
        $string = strval($string);
          if ($string) {
              $string = htmlspecialchars($string);
          }
          return $string;
    }

    static function strip($text) {
        $srting = array("query","select","from","delete","insert","update",";","'",'"',"^","|","\n","\r","\p","<",">");
        $result = trim(htmlspecialchars(strip_tags(str_replace($srting,"",$text)))); 
        return $result;
    }

  static function utf8_str_split($str) { 
  $split=1; 
  $array = array(); 
  for ( $i=0; $i < strlen( $str ); ){ 
    $value = ord($str[$i]); 
    if($value > 127){ 
      if($value >= 192 && $value <= 223) 
        $split=2; 
      elseif($value >= 224 && $value <= 239) 
        $split=3; 
      elseif($value >= 240 && $value <= 247) 
        $split=4; 
    }else{ 
      $split=1; 
    } 
      $key = NULL; 
    for ( $j = 0; $j < $split; $j++, $i++ ) { 
      $key .= $str[$i]; 
    } 
    array_push( $array, $key ); 
  } 
  return $array; 
} 

static function clearstr($str){ 
        $sru = 'ёйцукенгшщзхъфывапролджэячсмитьбю'; 
        $s1 = array_merge(self::utf8_str_split($sru), self::utf8_str_split(strtoupper($sru)), range('A', 'Z'), range('a','z'), range('0', '9'), array(' ')); 
        $codes = array(); 
        for ($i=0; $i<count($s1); $i++){ 
                $codes[] = ord($s1[$i]); 
        } 
        $str_s = self::utf8_str_split($str); 
        for ($i=0; $i<count($str_s); $i++){ 
                if (!in_array(ord($str_s[$i]), $codes)){ 
                        $str = str_replace($str_s[$i], '', $str); 
                } 
        } 
        return $str; 
} 

  /**
   * Перевод русского текста в транслит
   * @param string $str - форматируемый текст
   * @param integer $character_limiter - лимит символов
   * @return string
   */
  
  static function translite($str, $character_limiter = "60") {

      $str = self::clearstr($str);
      $str = trim($str);
      $str = strtolower($str);
      $str = self::character_limiter($str, $character_limiter, "");

      $translit = array("а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo", "Ё" => "yo", "ж" => "zh", "з" => "z",
          "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
          "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch",
          "ш" => "sh", "щ" => "sch", "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
          "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e", "Ж" => "zh", "З" => "z",
          "И" => "i", "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n", "О" => "o", "П" => "p",
          "Р" => "r", "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "c", "Ч" => "ch",
          "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya",
          " " => "-", "," => "");
      $str = strtr($str, $translit);
      return $str;
  }

  static function plural($count, $input) {
    $count = (int) $count;
    $l2 = substr($count, -2);
    $l1 = substr($count, -1);
    if ($l2 > 10 && $l2 < 20)
        return $input[0];
    else
        switch ($l1) {
            case 0: return $input[0];
                break;
            case 1: return $input[1];
                break;
            case 2: case 3: case 4: return $input[2];
                break;
            default: return $input[0];
                break;
        }
    return false;
    }

    static function getRusMonth($month)
    {
      if($month > 12 || $month < 1) return FALSE;
      $aMonth = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
      return $aMonth[$month - 1];
    }

    static function getRusMonthMin($month)
    {
      if($month > 12 || $month < 1) return FALSE;
      $aMonth = array('янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
      return $aMonth[$month - 1];
    }

    static function formatDate($unixtime, $format)
    {
        $dateDay = date('d', $unixtime);
        $dateMonth = date('m', $unixtime);
        $dateYear = date('Y', $unixtime);
        $time = date('H:i', $unixtime);

        switch ($format) {
            case 'y':
                return $dateDay . ' ' . self::getRusMonth($dateMonth) . ' ' . $dateYear;
                break;
            case 'm':
                return $dateDay . ' ' . self::getRusMonth($dateMonth);
                break;
            case 'mt':
                return $dateDay . ' ' . self::getRusMonth($dateMonth) . ' ' . $time;
                break;
            case 'mmt':
                return $dateDay . ' ' . self::getRusMonthMin($dateMonth) . ' ' . $time;
                break;
            
            default:
                # code...
                break;
        }
    }

    static function getDay($unixtime) 
    {
        $aDay = array('0' => 'ВС' ,'1' => 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ');
        $dateDay = date('w', $unixtime);
        return $aDay[$dateDay];
    }


}
?>