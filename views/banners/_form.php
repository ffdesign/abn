<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;

/**
 * @var yii\web\View $this
 * @var app\models\Banners $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, '_image')->fileInput() ?>

    <?php if($model->image): ?>
        <?php $file = 'images/banners/thumb-'.$model->image; ?>
        <img src="/<?=$file?>">   
    <?php endif; ?>
    
    <br>
    <label class="control-label" for="banners-place">Расположение</label>
    <select id="banners-place" class="form-control" name="Banners[place]">
        <option>-</option>
        <option value="Шапка">В шапке (1000x90)</option>
        <option value="Сквозной">Сквозной баннер (240х400)</option>
        <option value="Главная">На главной (240х400)</option>
        <? foreach ($newsCategory as $category): ?>
            <option value="<?=$category->title?>" <?= ($model->place == $category->title) ? 'selected' : '' ?>>Раздел <?=$category->title?> (468x60)</option>
        <? endforeach; ?>
    </select>
    

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
