<?php
/**
 * @var yii\web\View $this
 */

use app\helpers\TextHelper; 
$this->title = 'Архив за '.TextHelper::formatDate(strtotime($date), 'm').' | АБН';
?>  

<section class="lead_block">
   <? foreach ($model as $category): ?>
        <?
        if(!empty($category->archive)): 
            $find = $category->archive;
        else:
            $find = $category->oldNews;
        endif;
        ?>
        <? if($find): ?>
           <section class="razdel_block">
             <div class="razdel_caption" style="background-color: <?=$category->color?>"><?=$category->title?></div>

             <? foreach ($find as $news): ?>
                <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?>">
                    <? if($news->oneImage): ?>
                        <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
                        <a href="<?='/news/'.$category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                    <? endif; ?>

                    <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
                        <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?></span></div>
                        <a href="<?='/news/'.$category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
                        <div class="razdel_block_news_item_text"><?=$news->preview?></div>
                    </div>
                </div>
             <? endforeach; ?>

             <div class="razdel_more_link"><a href="<?='/news/category/'.$category->path?>"><span>Больше новостей</span></a></div>
           </section>
        <? endif; ?>
    <? endforeach; ?>
   
               
   
   
</section>
    