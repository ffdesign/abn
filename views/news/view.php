<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\TextHelper; 

/**
 * @var yii\web\View $this
 * @var app\models\News $model
 */


?>

<section class="article_block">
    <h1><?=$model->title?></h1>
    <div class="article_block_data">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td align="left" valign="top">
                        <div class="post_data"><span><?= TextHelper::formatDate($model->created, 'mt') ?></span><a href="/news/category/<?=$model->category->path?>" class="razdel_link1" style="background-color: <?=$model->category->color?>"><?=$model->category->title?></a></div>
                    </td>
                    <td valign="top" align="right">
                        <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki"></div> 
                     </td>
                </tr>
            </tbody>
        </table>
    </div>
    <section class="article_block_content">
        <? if($model->oneImage): ?>
            <div class="article_block_mainimg">
                <? $i = 0; foreach ($model->images as $img): $i++;?>
                    <? $image = '/images/news/'.$model->id.'/thumb/'.md5($img->image.$model->id).'-467x257.png'; ?>
                    <? if($i == 1): ?>
                        <a href="<?='/images/news/'.$model->id.'/'.$img->image ?>"  data-title="<?=$img->text ?>" data-lightbox="roadtrip"><img src="<?=$image?>"></a>
                    <? else: ?>
                        
                        <a href="<?='/images/news/'.$model->id.'/'.$img->image ?>"  data-title="<?=$img->text ?>" data-lightbox="roadtrip" class="hide"><img src="<?=$image?>"></a>
                    <? endif; ?>
                <? endforeach; ?>
                <? if($model->oneImage->author): ?><div class="article_block_imgauthor">Фото: <?=$model->oneImage->author ?></div><? endif; ?>
            </div>
        <? endif; ?>
        <div class="article_block_introtext">
            <p><?=$model->preview?></p>
        </div>
        <p><?=$model->text?></p>
    </section>

    <?  
        if(!isset($image)) $image = '';
        $this->title = array('title' => $model->title . ' | АБН', 'ogDescription' => $model->preview, 'ogImage' => $image); 
    ?>

    <div class="article_block_shared">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td valign="top" align="right">
                        <tr>
                        <td>
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=1401456516812642&version=v2.0";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-share-button" data-layout="button_count"></div>
                        </td>
                        <td>
                         <div id="vk_like"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like", {type: "mini"});
                        </script>
                        </td>
                        <td>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        </td>
                        </tr>
                       
                       
                        
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <? if(!empty($linkNews)): ?>
        <div class="article_block_title">Ссылки по теме</div>
        <div class="article_dop_link cls">
            <?  foreach ($linkNews as $lnews): ?>
                <div class="article_dop_link_item">
                    <div class="article_dop_link_item_data"><?= TextHelper::formatDate($lnews->created, 'mt') ?> / <?=$lnews->category->title?></div>
                    <a href="/news/<?=$lnews->category->path?>/<?=$lnews->path?>" class="article_dop_link_item_link"><?=$lnews->title?></a>
                </div>
            <? endforeach; ?>
        </div>
    <? endif; ?>
    
    <div class="article_block_title">Другие материалы рубрики</div>
    <div class="article_dop_link cls">
        <? foreach ($otherNews as $news): ?>
            <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?>">
                <? if($news->oneImage): ?>
                    <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
                    <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                <? endif; ?>

                <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
                    <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?></span></div>
                    <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
                    <div class="razdel_block_news_item_text"><?=$news->preview?></div>
                </div>
            </div>
        <? endforeach; ?>
        
    </div>
</section>