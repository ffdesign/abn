<?
use app\helpers\TextHelper;
$this->title = 'Все новости | АБН'; 
?>
<section class="news_block modify_news_block" style="padding-bottom:40px">
    <div class="article_block_title">Все новости</div>
    <section class="news_block_content">
        <div id="ContentZ">


            <? foreach ($model as $news): ?>
	            <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?>">
	                <? if($news->oneImage): ?>
	                    <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
	                    <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
	                <? endif; ?>

	                <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
	                    <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?></span></div>
	                    <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
	                    <div class="razdel_block_news_item_text"><?=$news->preview?></div>
	                </div>
	            </div>
	        <? endforeach; ?>
            
        </div>
	    <? if($model && $count > $pageSize): ?>
            <div class="loadNewsAll"></div>
            <div class="razdel_more_link" ><a href="#" id='loadNewsAll' alt="2"><span>Показать больше новостей</span></a></div>
	    <? endif; ?>

    </section>
</section>