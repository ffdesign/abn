<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;
use app\helpers\TextHelper;
use app\models\News;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NewsSearch $searchModel
 */

$this->title = 'Новости';

if(isset($categoryInfo)) {
    $this->params['breadcrumbs'][] = ['label' => 'Все новости', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $categoryInfo->title];
}
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;
?>
<div class="news-index">
    
    <b>Добавить новость в &nbsp;</b>
    <?php
            foreach ($categories as $category):
                echo  Html::a($category->title, ['create', 'id' => $category->id], ['class' => 'btn btn-success'])." ";
            endforeach;
    ?>
    <hr>
    <h3>Топ новостей</h3>
    <?php $topNews = new News; ?>
    <? if(isset($topNews)): ?>
        <table style="background-color: #dff0d8;">
            <thead>
                <th style=" padding: 5px">Топ 1</th>          
                <th style=" padding: 5px">Топ 2</th>          
                <th style=" padding: 5px">Топ 3</th>          
                <th style=" padding: 5px">Топ 4</th>          
            </thead>
            <tbody>
                <tr>
                <?php for ($i=1; $i < 5; $i++): ?>
                    <? if($topNews->getTopNews($i)): ?>
                        <td class="" width="100px" s>
                        <div style='position: relative; height: 320px; padding: 5px; <?= $topNews->getTopNews($i)->is_hot ? 'background-color: #fff9c2' : '' ?>'>
                            <?php 
                            $infoNews = $topNews->getTopNews($i);
                            if($infoNews):
                                if(isset($infoNews->oneImage->image)):
                                    $file = 'images/news/'.$infoNews->id.'/thumb/'.md5($infoNews->oneImage->image.$infoNews->id).'-306x180.png';
                                    echo "<img width='240' src=/$file><br><br>";
                                else:
                                    echo "<div style='width: 240px'>Изображения нет</div>";
                                endif;
                                echo "<span style='font-size: 12px; line-height: 3;'>".date('d.m.Y H:i', $infoNews['created'])."</span> <span class='label' style='background: ".$infoNews->category->color."'>".$infoNews->category->title."</span><br>";
                                echo Html::a('<b>'.$infoNews['title'].'</b>', "/news/update/".$infoNews->id)."<br>";

                                // echo Html::a('Убрать из топа', ['remove-top', 'id' => $infoNews['id']], ['class' => 'btn btn-primary', 'style' => 'margin-top: 5px; padding: 2px 8px !important; ']);
                                echo Html::a('Убрать из топа', ['remove-top', 'id' => $infoNews['id']], [
                                        'class' => 'btn btn-primary', 
                                        'style' => 'margin-top: 5px; padding: 2px 8px !important;',
                                        'onclick'=>"
                                             $.ajax({type:'POST', cache: false, url: '/news/remove-top/".$infoNews['id']."',
                                                success  : function(data) {
                                                    window.location.href = '/admin';
                                                }
                                            });
                                        return false;",
                                    ]);
                            else:
                                echo "Новости в топе пока нет";
                            endif;
                            ?>
                        </div>
                        </td>
                    <? else: ?>
                    <td><div style='position: relative; height: 320px; padding: 5px;'>Новости в топе пока нет</div></td>
                    <? endif; ?>
                <?php endfor; ?>
                </tr>
            </tbody>
        </table>
    <? endif; ?>
    <hr>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <hr>
   <?php
        if(isset($categoryInfo))
            echo Html::a('Добавить', ['create', 'id' => $_GET['id']], ['class' => 'btn btn-success']);
   ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Редактор',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->user->email;
                }, 
            ],
            [
                'attribute' => 'Изображение',
                'format' => 'raw',
                'value' => function ($model) { 
                    if($model->oneImage['image'])               
                        return '<img width="100" src='.'/images/news/'.$model->id.'/thumb/'.md5($model->oneImage['image'].$model->id).'.png>';
                    else 
                        return '<center>-</center>';
                },
            ],
            [
                'attribute' => 'Заголовок',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'Категория',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->category->title;
                },
            ],
            [
                'attribute' => 'Топ 1',
                'format' => 'raw',
                'value' => function ($model) {
                        if($model->is_top == 1)
                            return Html::a('<div id="top1'.$model->id.'"><div style="background: #dff0d8"><center><span class="glyphicon glyphicon-minus"></span></center></div></div>', ['remove-top', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/remove-top/$model->id',
                                        success  : function(data) {
                                            $(\"#top1$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>')
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<div id="top1'.$model->id.'"><center><span class="glyphicon glyphicon-plus" style="color: #428bca;"></span></center></div>', ['add-top', 'id' => $model->id, 'top' => 1], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/add-top/$model->id?top=1',
                                            success  : function(data) {
                                                $(\"#top4$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top2$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top3$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');

                                                $(\"#top1$model->id\").html('<div style=\"background: #dff0d8; color: #428bca;\"><center><span class=\"glyphicon glyphicon-minus\"></span></center></div>');

                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);
                        
                },
            ],
            [
                'attribute' => 'Топ 2',
                'format' => 'raw',
                'value' => function ($model) {                      
                       if($model->is_top == 2)
                            return Html::a('<div id="top2'.$model->id.'"><div style="background: #dff0d8"><center><span class="glyphicon glyphicon-minus"></span></center></div></div>', ['remove-top', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/remove-top/$model->id',
                                        success  : function(data) {
                                            $(\"#top2$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>')
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<div id="top2'.$model->id.'"><center><span class="glyphicon glyphicon-plus" style="color: #428bca;"></span></center></div>', ['add-top', 'id' => $model->id, 'top' => 1], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/add-top/$model->id?top=2',
                                            success  : function(data) {
                                                $(\"#top1$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top4$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top3$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');

                                                $(\"#top2$model->id\").html('<div style=\"background: #dff0d8; color: #428bca;\"><center><span class=\"glyphicon glyphicon-minus\"></span></center></div>');
                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);
                },
            ],
            [
                'attribute' => 'Топ 3',
                'format' => 'raw',
                'value' => function ($model) {                      
                       if($model->is_top == 3)
                            return Html::a('<div id="top3'.$model->id.'"><div style="background: #dff0d8"><center><span class="glyphicon glyphicon-minus"></span></center></div></div>', ['remove-top', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/remove-top/$model->id',
                                        success  : function(data) {
                                            $(\"#top3$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>')
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<div id="top3'.$model->id.'"><center><span class="glyphicon glyphicon-plus" style="color: #428bca;"></span></center></div>', ['add-top', 'id' => $model->id, 'top' => 1], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/add-top/$model->id?top=3',
                                            success  : function(data) {
                                                $(\"#top1$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top2$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top4$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');

                                                $(\"#top3$model->id\").html('<div style=\"background: #dff0d8; color: #428bca;\"><center><span class=\"glyphicon glyphicon-minus\"></span></center></div>');
                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);
                },
            ],
            [
                'attribute' => 'Топ 4',
                'format' => 'raw',
                'value' => function ($model) {                      
                       if($model->is_top == 4)
                            return Html::a('<div id="top4'.$model->id.'"><div style="background: #dff0d8"><center><span class="glyphicon glyphicon-minus"></span></center></div></div>', ['remove-top', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/remove-top/$model->id',
                                        success  : function(data) {
                                            $(\"#top4$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>')
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<div id="top4'.$model->id.'"><center><span class="glyphicon glyphicon-plus" style="color: #428bca;"></span></center></div>', ['add-top', 'id' => $model->id, 'top' => 1], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/add-top/$model->id?top=4',
                                            success  : function(data) {
                                                $(\"#top1$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top2$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                $(\"#top3$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>');
                                                
                                                $(\"#top4$model->id\").html('<div style=\"background: #dff0d8; color: #428bca;\"><center><span class=\"glyphicon glyphicon-minus\"></span></center></div>');
                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);
                },
            ],
            [
                'attribute' => 'Горячая новость',
                'format' => 'raw',
                'value' => function ($model) {             
                        if($model->is_hot == 1)
                            return Html::a('<div id="hot'.$model->id.'"><div style="background: #fff9c2"><center><span class="glyphicon glyphicon-minus"></span></center></div></div>', ['remove-hot', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/remove-hot/$model->id',
                                        success  : function(data) {
                                            $(\"#hot$model->id\").html('<center><span class=\"glyphicon glyphicon-plus\" style=\"color: #428bca;\"></span></center>')
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<div id="hot'.$model->id.'"><center><span class="glyphicon glyphicon-plus" style="color: #428bca;"></span></center></div>', ['add-hot', 'id' => $model->id], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/add-hot/$model->id',
                                            success  : function(data) {
                                                $(\"#hot$model->id\").html('<div style=\"background: #fff9c2; color: #428bca;\"><center><span class=\"glyphicon glyphicon-minus\"></span></center></div>');
                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);

                       if($model->is_hot == 1)
                            return Html::a('<div style="background: #fff9c2"><center><span class="glyphicon glyphicon-minus"></span></center></div>', ['remove-hot', 'id' => $model->id]);
                        else 
                            return Html::a('<center><span class="glyphicon glyphicon-plus"></span></center>', ['add-hot', 'id' => $model->id]); 
                },
            ],
            [
                'attribute' => 'Добавлена',
                'format' => 'raw',
                'value' => function ($model) {                      
                            return date('d.m.Y H:i', $model->created);
                },
            ],
            [
                'attribute' => 'Обновлена',
                'format' => 'raw',
                'value' => function ($model) {                      
                            return !empty($model->updated) ? date('d.m.Y H:i', $model->updated) : '<center>-</center>';
                },
            ],
            [
                'attribute' => 'Отложенная публикация',
                'format' => 'raw',
                'value' => function ($model) {                      
                            return !empty($model->postponed) ? '<b>'.date('d.m.Y H:i', $model->postponed).'</b>' : '<center>-</center>';
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        if($model->is_visible == 1)
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['off-visible', 'id' => $model->id], [
                                'title' => 'Не показывать новость',
                                'id' => "link$model->id",
                                'onclick'=>"
                                     $.ajax({type:'POST', cache: false, url: '/news/off-visible/$model->id',
                                        success  : function(data) {
                                            $(\"#link$model->id\").addClass('link_visible');
                                            $('.modal-body').html(data);
                                            $('#infoModal').modal({
                                              keyboard: true
                                            });
                                        }
                                    });
                                return false;",
                            ]);
                        else 
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['on-visible', 'id' => $model->id], [
                                    'title' => 'Показывать новость',
                                    'class' => 'link_visible',
                                    'id' => "link$model->id",
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/on-visible/$model->id',
                                            success  : function(data) {
                                                $(\"#link$model->id\").removeClass('link_visible');
                                                $('.modal-body').html(data);
                                                $('#infoModal').modal({
                                                  keyboard: true
                                                });
                                            }
                                        });
                                    return false;",
                                ]);
                    }, 
                ],
            ],
        ],
    ]); ?>  

    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Хорошо</button>
          </div>
        </div>
      </div>
    </div>
</div>
