<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imagine\Image;
use yii\jui\DatePicker;
use dosamigos\datetimepicker\DateTimePicker;
use yii\helpers\ArrayHelper;
use app\models\NewsCategories;
/**
 * @var yii\web\View $this
 * @var app\models\News $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#home" role="tab" data-toggle="tab">Основная информация</a></li>
      <li><a href="#prop" role="tab" data-toggle="tab">Дополнительная информация</a></li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane active" id="home">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <div class="form-group field-news-category required">
                <label class="control-label" for="news-category">Категория</label>
                <?= Html::activeDropDownList($model, 'id_category', ArrayHelper::map(NewsCategories::find()->all(), 'id', 'title'), array('class' => 'form-control', 'id' => 'news-category')) ?>
            </div>
            
            <?php if(!$model->isNewRecord): ?>
                    <?= $form->field($model, 'path')->textInput(['maxlength' => 255]) ?>
            <? endif; ?>

            <?= $form->field($model, 'preview')->textarea() ?>

            <label>Текст</label>
            <?= yii\imperavi\Widget::widget([
                    'model' => $model,
                    'attribute' => 'text',
                    'options' => [
                        'minHeight' => '200',
                        'lang' => 'ru',
                        'imageUpload' => '/site/upload-image',
                        'fileUpload' => '/site/upload-file',
                    ],
            ]); ?>

            <hr>
            <? if($model->images): ?>
                <label>Загруженные изображения</label><br>
                <input type="hidden" name="News[image_new]" value="1">
                <? foreach ($model->images as $image): ?>
                    <div class="image<?=$image->id?>">
                        <? $file = 'images/news/'.$model->id.'/thumb/'.md5($image->image.$model->id).'.png'; ?>
                        <img src="/<?=$file?>">
                        <br>
                        <label for="image-author<?=$image->id?>">Автор изображения</label>
                        <input type="text" id="image-author<?=$image->id?>" class="form-control" name="News[image_author][<?=$image->id?>]" value="<?=$image->author?>">
                        <p></p><label for="image-preview<?=$image->id?>">Описание</label>
                        <input type="text" id="image-preview<?=$image->id?>" class="form-control" name="News[image_preview][<?=$image->id?>]" value="<?=$image->text?>">
                        <br>
                        <?= Html::a('Удалить <span class="glyphicon glyphicon-trash"></span>', ['delete-image', 'id' => $image->id], [
                                    'title' => 'Удалить изображение',
                                    'onclick'=>"
                                         $.ajax({type:'POST', cache: false, url: '/news/delete-image/$image->id',
                                            success  : function(data) {
                                                $(\".image$image->id\").remove();
                                            }
                                        });
                                    return false;"
                            ]);
                        ?>
                    <hr>
                    </div>
                <? endforeach; ?>
            <? else: ?>
                <label>Изображение</label>
                <input type="hidden" name="News[image_new]" value="1">
                <input type="file" id='news-image0' class="news-image-file" name="News[image][]">
                <label for="image-author0">Автор изображения</label>
                <input type="text" id="image-author0" class="form-control" name="News[image_authorc][]">
                <label for="news-image-preview0">Описание</label>
                <input type="text" id='news-image-preview0' class="form-control" name="News[image_previewc][]">
            <? endif; ?>
            <div class='input-image'></div>
            <br>
            <a id='add-input-image' href='#'>Добавить еще изображение <span class="glyphicon glyphicon-plus"></span></a>
            <hr>

            <?= $form->field($model, 'tags')->textInput(['maxlength' => 255]) ?>
            
            <?= $form->field($model, 'position')->textInput() ?>

            <?= $form->field($model, 'is_top')->dropDownList(
                array(
                    ''=>'-',
                    '1'=>'Топ 1',
                    '2'=>'Топ 2',
                    '3'=>'Топ 3',
                    '4'=>'Топ 4',
                    )
                );
            ?>

            <?= $form->field($model, 'updateDate')->checkbox(['id' => 'updateDate']) ?>

            <?= $form->field($model, 'created')->widget(DatePicker::className(), ['clientOptions' => ['dateFormat' => 'dd.mm.yy'], 'language'=> 'ru']) ?>

            <?= $form->field($model, 'is_hot')->checkbox() ?>

            <?= $form->field($model, 'is_visible')->checkbox() ?>
        </div>

        <div class="tab-pane" id="prop">
            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
        </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
          Отложенная публикация
        </button>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title" id="myModalLabel">Отложенная публикация</h4>

                </div>
                <div class="modal-body">
                    <div class="input-group date" id="datetimepicker1">
                        <input name="News[postponed]" type="text" class="form-control" / value="<?= $model->postponed ?>">
                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Применить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
