<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\News $model
 */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Все новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $categoryInfo->title];
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index', 'id' => $_GET['id']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
