<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\NewsCategories;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\search\NewsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <?php  echo $form->field($model, 'text')->label('Поиск по новостям') ?>
    <?php  echo $form->field($model, 'searchontitle')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Сброс', ['/news/index'], ['class' => 'btn btn-primary']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
