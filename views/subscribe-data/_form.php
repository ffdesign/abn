<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SubscribeData $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="subscribe-data-form">

    <?php $form = ActiveForm::begin(); ?>
	<ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#home" role="tab" data-toggle="tab">Новости</a></li>
      <li><a href="#prop" role="tab" data-toggle="tab">Подписчики</a></li>
    </ul>
    <br>
    <div class="tab-content">
        <div class="tab-pane active" id="home">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>#</th>
                  <th>Изображение</th>
                  <th>Заголовок</th>
                  <th>Топ 1</th>
                  <th>Топ 2</th>
                  <th>Топ 3</th>
                  <th>Топ 4</th>
                  <th>Горячая новость</th>
                  <th>Добавлена</th>
                  <th>Обновлена</th>
                </tr>
              </thead>
              <tbody>
              	<?php foreach ($news as $row): ?>
        	        <tr>
        	          <th><input name="SubscribeData[news][<?=$row->id?>]" type="checkbox"></th>
        	          <th><input name="SubscribeData[news_index]" value="<?=$row->id?>" type="radio"></th>
                    <th>
                        <? $file = 'images/news/'.$row->id.'/thumb/'.md5($row->oneImage['image'].$row->id).'.png'; ?>
                        <? if($row->oneImage['image']): ?>
                            <img width="100" src="/<?=$file?>">
                        <? else: ?>
                            <center>-</center>
                        <? endif; ?>
                    </th>
        	          <th><?= $row->title; ?></th>
        	          <th><?= $row->is_top == 1 ? '<div style="background: #dff0d8; text-align: center">Да</div>' : '<center>Нет</center>'; ?></th>
        	          <th><?= $row->is_top == 2 ? '<div style="background: #dff0d8; text-align: center">Да</div>' : '<center>Нет</center>'; ?></th>
        	          <th><?= $row->is_top == 3 ? '<div style="background: #dff0d8; text-align: center">Да</div>' : '<center>Нет</center>'; ?></th>
        	          <th><?= $row->is_top == 4 ? '<div style="background: #dff0d8; text-align: center">Да</div>' : '<center>Нет</center>'; ?></th>
        	          <th><?= $row->is_hot ? '<div style="background: #fff9c2; text-align: center">Да</div>' : '<center>Нет</center>'; ?></th>
        	          <th><?= date('d.m.Y', $row->created); ?></th>
        	          <th><?= $row->updated ? date('d.m.Y', $row->updated) : '<center>-</center>'; ?></th>
        	        </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="tab-pane" id="prop">
            <? foreach ($emails as $email): ?>
                <label><input name="SubscribeData[emails][]" type="checkbox" value="<?=$email->email?>" checked> <?=$email->email?></label><br>
            <? endforeach; ?>
        </div>

    </div>


</div>
