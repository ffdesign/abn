<?php

use yii\helpers\Html;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\SubscribeDataSearch $searchModel
 */

$this->title = 'Рассылка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-data-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?> <?= Html::a('Подписчики', ['subscribe/admin'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Заголовок',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->title;
                },
            ],
            [
                'attribute' => 'Новостей в рассылке',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->count_news;
                },
            ],
            [
                'attribute' => 'Кол-во адресатов',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->count_sends;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>

</div>
