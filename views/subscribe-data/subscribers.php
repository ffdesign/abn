<?
use yii\helpers\Html;
$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = ['label' => 'Рассылка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-data-index">
    <h1><?= Html::encode($this->title) ?></h1>
	<table class="table table-striped table-bordered">
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Электронная почта</th>
	        </tr>
	    </thead>
	    <tbody>
	        
        	<? $i = 0; foreach ($model as $row): $i++;?>
	        	<tr data-key="1">
	        		<td><?= $i ?></td>  
	            	<td><?= $row->email ?></td>
	            </tr>  
        	<? endforeach; ?>
	            
	        
	    </tbody>
	</table>
</div>