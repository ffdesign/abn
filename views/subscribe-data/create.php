<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SubscribeData $model
 */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Рассылка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'news' => $news,
        'emails' => $emails,
    ]) ?>

</div>
