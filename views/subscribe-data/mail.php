<?
    use app\helpers\TextHelper;
    ?>
<meta charset="UTF-8"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td bgcolor="#1e3f9a" align="center" style="width: 1316px; height: 500px; color: #FFFFFF">

                <table border="0" style="width: 700px; height: 200px;  color: #FFFFFF; background-image: url(http://abn.info/images/mail/top_bg.png); " >
                    <tr>
                        <td style="padding-left: 31px;  display: block; ">
                            <span style="background-color: red; font-family: PT Sans; font-weight: bold; padding: 0px 4px;text-transform: uppercase; font-size: 12px"><?= $mailInfo['day'] ?></span>
                              <span style="padding-right: 4px; font-family: PT Sans; font-size: 12px;"><?= $mailInfo['date'] ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 31px;  display: block; padding-right: 20px">
                            <img src="http://abn.info/images/mail/logo_abn.png">
                            <hr style="color: #4b61a0;  background-color:#4b61a0; border:0px none; height:1px; clear:both; display: block;margin-top: 15px;margin-bottom: 0px;">
                        </td>
                    </tr>
            <!--         <tr>
                        <td></td>
                    </tr> -->
                    <tr valign="top">
                        <td style="font-size: 24px; font-family: PT Sans; font-weight: bold;  text-transform: uppercase; padding-left: 28px; "><?= $mailInfo['title'] ?></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
                      
                <!-- Блок для главной новости -->
                <table style="width: 700px; height: 220px; background: #2c2c2c;">
                    <tr>
                    	<? if($sendNewsOne->oneImage): ?>
	                        <td style="padding-left: 28px; width: 200px">
		                            <?php 
		                                $file = "http://abn.info/images/news/".$sendNewsOne->id."/thumb/".md5($sendNewsOne->oneImage->image.$sendNewsOne->id).'-306x180.png';
		                                ?>
		                            <img src="<?= $file ?>" width="263" height="152">
	                        </td>
                        <? endif; ?>
                        <td style="width: 370px; <?=!$sendNewsOne->oneImage ? 'padding-left: 28px;' : '' ?>">
                            <span style="color: #ffffff; font-size: 22px; font-weight: bold; width: 360px; display: block; padding-bottom: 5px;">
                                <a target="_blank" style="color: #ffffff; font-family: PT Sans; line-height: 25px;  text-decoration: none;" href="http://abn.info/news/<?=$sendNewsOne->category->path?>/<?=$sendNewsOne->path?>"><?=$sendNewsOne->title?></a>
                            </span>

                            <span style="color: #aeaeae; font-family: arial; font-size: 12px; width: 360px; display: block; padding-bottom: 5px;"><?=$sendNewsOne->preview?></span>
                            <span style="font-size: 10px; font-family: PT Sans; color: #8f8f8f; padding-top: 2px; background-image: url('http://abn.info/images/mail/clock_white.png');background-position: left center;background-repeat: no-repeat;padding-left: 19px;"><?=TextHelper::formatDate($sendNewsOne->created, 'mt')?></span> 
                            <a target="_blank" href="http://abn.info/news/category/<?=$sendNewsOne->category->path?>" style="border-radius: 2px;font-family: 'Arial';letter-spacing: 1px;font-size: 7px;color: white;font-weight: bold;padding: 3px 5px;text-decoration: none;text-transform: uppercase;line-height: 20px; background-color: #306cc7"><?=$sendNewsOne->category->title?></a>
                        </td>
                    </tr>
                </table>
                
                <!-- Остальные новости -->
                <table style="width: 700px; background: #ffffff; padding-top: 10px;">
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <? $i = 0; foreach ($sendNews as $news): $i++; ?>  

                    <tr >
                    	<? if($news->oneImage): ?>
                        <td style="padding-left: 28px; width: 140px; vertical-align: top;">
	                            <?php 
	                                $file = "http://abn.info/images/news/".$news->id."/thumb/".md5($news->oneImage->image.$news->id).'-306x180.png';
	                                ?>
	                            <img src="<?= $file ?>" width="117" height="77">
                        </td>
                        <? endif; ?>
                        <td style="width: 540px; <?=!$news->oneImage ? 'padding-left: 28px; ' : '' ?> padding-right: 28px; vertical-align: top;" colspan="2">
                            <span style="font-size: 10px; display: block; padding-bottom: 2px; font-family: PT Sans; color: #948c8c; background-image: url('http://abn.info/images/mail/clock.png');background-position: left center;background-repeat: no-repeat;padding-left: 19px;">
                                <?=TextHelper::formatDate($news->created, 'mt')?> / 
                            <span style="text-transform: uppercase;">
                                <a target="_blank" display: block; padding-bottom: 5px; style="color: #948c8c; text-decoration: none; font-family: PT Sans; line-height: 16px;" href="http://abn.info/news/category/<?=$news->category->path?>"><?=$news->category->title?></a>
                            </span>
                            </span> 
                            <span style="color: #000000; font-size: 16px; font-weight: bold; font-family: PT Sans; display: block; padding-bottom: 5px;">
                                <a target="_blank" style="color: #000000; text-decoration: none; font-family: PT Sans;  display: block; line-height: 18px;" href="http://abn.info/news/<?=$news->category->path?>/<?=$news->path?>"><?=$news->title?></a>
                            </span>
                            <span style="color: #000000; font-family: arial; font-size: 12px; "><?=$news->preview?></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 28px; padding-bottom: 20px; padding-right: 28px;" colspan="2">
                            <?= $mailInfo['countNews'] == $i ? '' : '<hr style="color: #dedede;  background-color:#dedede; border:0px none; height:1px; clear:both; display: block;margin-top: 15px;margin-bottom: 0px;">' ?>
                        </td>
                    </tr>
                    <? endforeach; ?>
                    <tr>
                        <td style="padding-left: 28px; " colspan="2">
                            <a target="_blank" href="http://abn.info/"><img src="http://abn.info/images/mail/more-news.png"></a>
                            <br>
                            <br>
                        </td>
                    </tr>
                </table>
               
                
                <!-- Подвал -->
                <table style="width: 700px; height: 138px; background: #2c2c2c;">
                    <tr>
                        <td style="padding-left: 28px; ">
                            <span style="font-family: PT Sans; font-weight: bold; font-size: 12px; color: #ffffff">
                            Расслыка ЗАО «Агентство бизнес-новостей». <a style="color: #ffffff; " target="_blank" href="http://abn.info/unsubscribe">Отписаться от этой рассылки</a>
                            </span>
                            <br><br>
                            <span style="font-family: arial; font-size: 9px; color: #818181; padding-top: 10px">
                            Все права на материалы и новости, опубликованные на сайте, охраняются в соответствии с законодательством РФ. Допускается цитирование
                            без согласования с редакцией не более 50% от объема оригинального материала, с обязательной прямой гиперссылкой на страницу, с которой материал заимствован. Гиперссылка должна размещаться непосредственно в тексте, воспроизводящем оригинальный материал, до или после цитируемого блока.
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>