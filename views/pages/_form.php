<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imperavi\Widget;

/**
 * @var yii\web\View $this
 * @var app\models\Pages $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#home" role="tab" data-toggle="tab">Основная информация</a></li>
      <li><a href="#prop" role="tab" data-toggle="tab">Дополнительная информация</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="home">
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        
        <? if(!$model->isNewRecord): ?>
            <?= $form->field($model, 'path')->textInput(['maxlength' => 255]) ?>
        <? endif; ?>

        <label>Текст</label>
        <?= yii\imperavi\Widget::widget([
                'model' => $model,
                'attribute' => 'text',
                'options' => [
                    'lang' => 'ru',
                    'imageUpload' => '/site/upload-image',
                    'fileUpload' => '/site/upload-file',
                ],
        ]); ?>

        <?= $form->field($model, 'is_visible')->checkbox() ?>
      </div>

      <div class="tab-pane" id="prop">
        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


