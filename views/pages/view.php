<?php

use yii\helpers\Html;


$this->title = $model->title.' | АБН';
?>
<section class="article_block">
    <h1><?= $model->title ?></h1>
    <section class="article_block_content">
        <?= $model->text ?>
    </section>
</section>