<?php
use yii\helpers\Html;
use app\helpers\TextHelper; 
use app\helpers\YiiHelper; 

$this->title = 'Результаты поиска | АБН';
?>  
<section class="news_block modify_news_block">
    <section class="big_search_form">
        <div class="search_block_title">Результаты поиска</div>
        <form id="bigsearch" action="" method="get">
        <div class="search_block_search_on_title"><input id="searchOnTitle" name="ontitle" type="checkbox" <?= isset($_GET['ontitle']) ? 'checked' : '' ?>> Искать в заголовках</div>
            <input type="text" name="search" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>" placeholder="Поиск по сайту" class="biginput">
        </form>
        <div class="subsearch_menu">
            <? if(isset($arCategory)): ?>
                <ul class="search_menu">
                    <? $i = 0; foreach ($listCategory as $category): $i++; ?>
                        <li data-idcategory="<?=$category->id?>" class="filterCat <?= (array_search($category->title, $arCategory) != '') ? 'act' : '' ?>"><a href="#" class="menuitem<?=$i?>"><?= $category->title; ?></a></li>
                    <? endforeach; ?>
                </ul>
            <? endif; ?>
        </div>
    </section>

    
    <?  if(!empty($model)): ?>
        <div class="sort">
            <a href="?search=<?=$_GET['search']?>&date=<?= ($sortDate == 'asc') ? 'desc' : 'asc' ?>"><div class="sortDate <?= isset($_GET['date']) ? 'active' : '' ?>">по дате <span class="<?= ($sortDate == 'asc') ? 'down' : 'up' ?>"></span></div></a>
            <a href="?search=<?=$_GET['search']?>&relevance=<?= ($sortRelevance == 'asc') ? 'desc' : 'asc' ?>"><div class="sortRelevance <?= isset($_GET['relevance']) ? 'active' : '' ?>">по релевантности <span class="<?= ($sortRelevance == 'asc') ? 'down' : 'up' ?>"></span></div></a>
        </div>
        <section class="news_block_content">
             <? foreach ($model as $news): ?>
                <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?> cat<?=$news->category->id?>">
                    <? if($news->oneImage): ?>
                        <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
                        <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                    <? endif; ?>

                    <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
                        <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?> / <span style="text-transform:uppercase"><a href="/news/category/<?=$news->category->path?>" class="razdel_link_simple"><?=$news->category->title?></a></span></span></div>
                        <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
                        <div class="razdel_block_news_item_text"><?=$news->preview?></div>
                    </div>
                </div>
            <? endforeach; ?>

            <? if($model && $count > $pageSize): ?>
                <div class="loadNewsSearch"></div>
                <div class="razdel_more_link" ><a href="#" id='loadNewsSearch' alt="2"><span>Показать больше новостей</span></a></div>
            <? endif; ?>
        </section>
    <? else: ?>
        <div class="search_block_title" style="color: #606060; margin-left: 63px">Ничего не найдено</div>
    <? endif; ?>
</section>