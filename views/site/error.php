<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = $name . 'Страница не найдена (404) | АНРТ';
?>

<section class="article_block">
    <h1>Страница не найдена</h1>
    <section class="article_block_content">
        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
    </section>
</section>