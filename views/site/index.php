<?php
/**
 * @var yii\web\View $this
 */
$this->title = 'АБН';
use app\helpers\TextHelper; 
use app\helpers\YiiHelper; 
?>  

<section class="lead_block">
   <? foreach ($model as $category): ?>
        <? if($category->news): ?>
           <section class="razdel_block">
             <div class="razdel_caption" style="background-color: <?=$category->color?>"><?=$category->title?></div>
             <? foreach ($category->news as $news): ?>
                <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?>">
                    <? if($news->oneImage): ?>
                        <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
                        <a href="<?='/news/'.$category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                    <? endif; ?>

                    <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
                        <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?></span></div>
                        <a href="<?='/news/'.$category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
                        <div class="razdel_block_news_item_text"><?=$news->preview?></div>
                    </div>
                </div>
             <? endforeach; ?>
            
            <? $banner = YiiHelper::getBanners($news->category->title);  ?>
            <? if($banner): ?>
                <div class="razdel_banner_468"><a target="_blank" href="<?=$banner['url']?>"><img width="465" height="60" src="/images/banners/<?=$banner['image']?>"></a></div>
            <? endif; ?>
            <div class="razdel_more_link"><a href="<?='/news/category/'.$category->path?>"><span>Больше новостей</span></a></div>
           </section>
        <? endif; ?>
    <? endforeach; ?>
   
               
   
   
</section>
    