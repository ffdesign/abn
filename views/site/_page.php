<?php

use yii\helpers\Html;
use app\helpers\TextHelper; 
use yii\widgets\LinkPager;
/**
 * @var yii\web\View $this
 * @var app\models\NewsCategories $model
 */

?>

<? foreach ($model as $news): ?>
    <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?> cat<?=$news->category->id?>">
        <? if($news->oneImage): ?>
            <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
            <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
        <? endif; ?>

        <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
            <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?> / <span style="text-transform:uppercase"><a href="/news/category/<?=$news->category->path?>" class="razdel_link_simple"><?=$news->category->title?></a></span></span></div>
            <a href="<?='/news/'.$news->category->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
            <div class="razdel_block_news_item_text"><?=$news->preview?></div>
        </div>
    </div>
<? endforeach; ?>