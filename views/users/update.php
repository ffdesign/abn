<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */

$this->title = 'Редактировать: ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Редакция', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
