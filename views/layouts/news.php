<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\HomeWidget;
use app\widgets\NewsScrollBlockWidget;
use app\widgets\MiniMenuWidget;
use app\widgets\MetaWidget;
use app\helpers\YiiHelper; 
use app\helpers\TextHelper; 
use yii\caching\Cache;
use app\models\Setting;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
// AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title['title']) ?></title>
    <meta content="width=device-width, initial-scale=0.3,maximum-scale=3,user-scalable=1" name="viewport">
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
    <meta property="og:locale" content="ru_RU">
    <meta property="og:site_name" content="АБН">
    <meta property="fb:app_id" content="1401456516812642" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?= Html::encode($this->title['title']) ?>">
    <meta property="og:url" content="http://abn.info<?= Yii::$app->request->getUrl() ?>">
    <meta property="og:description" content="<?= Html::encode($this->title['ogDescription']) ?>">
    <meta property="og:image" content="http://abn.info<?= Html::encode($this->title['ogImage']) ?>">
    <meta property="og:image:type" content="image/png">
    <!-- <base href="http://www.abn.info/"></base> -->
    
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:700|PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="/styles/lightbox.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="/styles/jquery.jscrollpane.css"/>
    <link rel="stylesheet" href="/styles/prettyPhoto.css"  />
    <!--[if IE]><link rel="stylesheet" href="/styles/style_ie.css" type="text/css" media="screen, projection" /><![endif]--> 
    
    <script src="/js/jquery-1.10.2.min.js"></script> 
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.mousewheel.js"></script> 
    <script src="/js/modernizr-2.5.3.min.js"></script>
    <script src="/js/jquery.hc-sticky.js"></script>
    <script src="/js/jquery.prettyPhoto.js"></script>
    <script src="/js/script.js"></script>
    <script src="/js/lightbox/lightbox.js"></script> 
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1401456516812642',
          xfbml      : true,
          version    : 'v2.2'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/ru_RU/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

    <script type="text/javascript">
      VK.init({apiId: 4527421, onlyWidgets: true});
    </script>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/fav.png" type="image/x-icon" /> 
     
        
</head>

<body>
<?php $this->beginBody() ?>
    <section id="miniheader">
       <div class="miniheaderwrap">
         <table cellpadding="0" cellspacing="0" border="0" width="100%">
         <tr>
         <td align="left" valign="top">
            <div class="minilogo"><a href="/"><img src="/images/minilogo.png" title="АБН" border=0 ></a></div>
         </td>   
         <td align="left" valign="top">
            <nav>
                 <ul class="minimainmenu">
                    <?= MiniMenuWidget::widget(); ?>      
                </ul>
            </nav>       
         </td>   
         <td align="left" valign="top"><a href="#" class="datepicker_link_mini"><span></span></a></td>   
         <td align="left" valign="top"><a href="#" class="searchform_link_mini"><span></span></a></td>
         </tr>
         </table>
         <div class="search_form_mini">
                        <form id="search_mini" action="/search" method="get" class="search">
                        <input type="text" name="search" value="" placeholder="Что ищем?">
                        <input type="submit" value="">
                        <a href="#" class="close_mini_search"></a>
                        </form>         
         </div>      
       </div>   
    </section>  

    
<div id="wrapper">
<div id="wrapperpage">

    <header id="header">
        
        <section class="today_area">
        <div class="today_block">
            <? if(isset($_GET['date'])): ?>
                <span class="day_name"><?= TextHelper::getDay(strtotime($_GET['date'])) ?></span> <span><?= TextHelper::formatDate(strtotime($_GET['date']), 'm') ?></span>
            <? else: ?>
                <span class="day_name"><?= TextHelper::getDay(time()) ?></span> <span><?= TextHelper::formatDate(time(), 'm') ?></span>
            <? endif; ?>
            <?php
                $lastUpdate = Yii::$app->getCache()->get('lastUpdate');
                if ($lastUpdate === false || Yii::$app->getCache()->get('updateNews')) {
                    $lastUpdate = date('H:i', time());
                    $updateActualization = Setting::find()->where(['id' => 1])->one();
                    $updateActualization->actualization = $lastUpdate;
                    $updateActualization->save();
                    Yii::$app->getCache()->set('lastUpdate', $lastUpdate);
                }
            ?>
            <span class="last_post_date">актуализация - <?= $lastUpdate ?> мск</span>
          </div>
          <div class="rss">
            <span class="rss"><a target="_blank" href="/rss"><span class="icon">RSS</span></a></span>
          </div>
        </section>
        
        <section class="cls">
           <div class="logo"><a href="/"><img src="/images/logo.png" title="АБН" border=0 ></a></div>
           <div class="submenu">
              <div class="cls">


                 <div class="area_search">
                        <form id="search" action="/search" method="get" class="search">
                        <input type="text" name="search" value="" placeholder="Поиск по сайту">
                        <input type="submit" value="">
                        </form>
                 </div>           
                 
                 <div class="area_celendar">
                  <a href="#" class="datepicker_link">Архив</a>
                 </div>
                 
                 <div class="area_curs">
                    <?php
                        $lastCurrency = Yii::$app->getCache()->get('lastCurrency');
                        if ($lastCurrency === false || Yii::$app->getCache()->get('updateCurrency')) {
                            $rate = YiiHelper::getCurrency();
                            $lastCurrency = '<div class="topkurs dollar">'.$rate[0].'<span class="'.$rate[1].'"></span></div><div class="topkurs euro">'.$rate[2].'<span class="'.$rate[3].'"></span></div>';
                            Yii::$app->getCache()->set('lastCurrency', $lastCurrency);
                            Yii::$app->getCache()->set('updateCurrency', false);
                        }
                    ?>
                   <div class="area_curs_title">Официальные курсы:</div>
                   <div class="area_curs_block"><div style="clear: both"><?= $lastCurrency ?> </div></div>
                 </div>

                 
              </div>                
           </div>
        </section>

        <?= HomeWidget::widget(); ?>
        <?//= $_GET['category'] ?>
        <section <?= (isset($_GET['date']) OR Yii::$app->request->getUrl() == '/') ? "id=''" : "id='content'" ?> <?= (Yii::$app->request->getUrl() !== '/') ? 'class="content_index_topblock1"' : 'id="index_main_content_block"' ?>>
           <table cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td align="left" valign="top">
                <?=$content?>
            </td>    
            <td align="left" valign="top">
               <?= NewsScrollBlockWidget::widget(); ?>
            </td>    
            <td align="left" valign="top">
               <section class="partner_block <?= (Yii::$app->request->getUrl() !== '/') ? 'partner_block_nomargin' : '' ?>">
                    <? if(Yii::$app->request->getUrl() !== '/' && !isset(Yii::$app->request->get()['date'])): ?>
                        <? $banner = YiiHelper::getBanners('Сквозной');  ?>
                        <? if($banner): ?>
                            <a href="<?=$banner['url']?>" target="_blank"><img src="/images/banners/240x400-<?=$banner['image']?>" border="0" width="240" height="400"></a>
                        <? endif; ?>
                    <? endif; ?>

                    
                       
               </section>
            </td>    
            </tr>
           </table>
        </section>
        
        

    </section><!-- #content-->
</div><!-- #wrapperpage -->
</div><!-- #wrapper -->

<footer id="footer">
    <nav>
        <ul class="footmenu">
            <li><a href="/pages/ob-agentstve" >Об агентстве</a></li>
            <li><a href="/forum">Форум</a></li> 
            <li><a href="/contacts" >Контакты</a></li>
            <li><a target="_blank" href="/rss" ><span class="icon">RSS-подписка</span></a></li> 
        </ul>
    </nav>
    <section class="footcontent">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
    <td align="left" valign="top"><div class="footlogo"><a href="/"><img src="/images/footlogo.png" title="АБН" border=0 ></a></div>
      <div class="cross-link">
        </div>
    </td>     
    <td align="left" valign="top">
    <div class="footcopy"><p><?=YiiHelper::getTextblock("Подвал: копирайт")?></p></div>
     <div class="foottext"><?=YiiHelper::getTextblock("Подвал: инфа")?></div>
        <div class="footbanners">
            <?=YiiHelper::getTextblock("Подвал: счетчики")?>
        </div>
    </td>
    </tr>
    </table>
    </section>
</footer><!-- #footer -->
<a class="scrollup" href="#"></a>
<div id="fade"></div>

    <script src="/js/picker.js"></script>
    <script src="/js/picker.date.js"></script>
    
    <link rel="stylesheet" href="/js/themes/classic.css" >
<link rel="stylesheet" href="/js/themes/classic.date.css" >
    
<script type="text/javascript">
$(document).ready(function(){
            
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    
    
 var shrinkHeader = 300;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('#miniheader').show();
        }
        else {
           $('#miniheader').hide();
        }
  });
function getCurrentScroll() {
    return window.pageYOffset;
    }   
    
    
    var rscroll = getCurrentScroll();
      if ( rscroll >= shrinkHeader ) {
           $('#miniheader').show();
        }
        else {
           $('#miniheader').hide();
        }   
    
    $('.datepicker_link, .datepicker_link_mini').pickadate({
        onSet: function() {
          var zref = this.get();
            if (zref=='') return false;
            //alert(zref);
          window.location = '/news/archive/'+zref;
        },
        onClose: function() { $('#fade').fadeOut(); } ,
        monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        weekdaysShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        firstDay: 1,
        clear: '',
        today: '',
        //highlight: '2014-05-05',
        //picker.set('highlight', [2014,5,20]),
        format: 'dd-mm-yyyy',
        max: true     
    });
    
    $('.datepicker_link_mini, .datepicker_link').click(function(){ $('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); return false });
    
        
    
     var picker = $('.datepicker_link, .datepicker_link_mini').pickadate('picker');
    //picker.set('select', [2013,3,20], { muted: true });

    $("a[rel^='prettyPhoto']").prettyPhoto({  social_tools: '<div class="pp_social"></div>' });
    
    $('.searchform_link_mini').click(function(){ 
      $('.search_form_mini').show();
      return false 
    }); 
    $('.close_mini_search').click(function(){ 
      $('.search_form_mini').hide();
      return false 
    });
    
    
    
    $('.lenta_block_scroll').css("height", (($(window).height())-160)  + "px"); 
    $(window).resize(function() {
        $('.lenta_block_scroll').css("height", (($(window).height())-160)  + "px"); 
    });
    
    $('.lenta_block_scroll').jScrollPane();
    $('.lenta_block').hcSticky({top: 40});
});
</script>

<!-- <?php $this->endBody() ?> -->
</body>
</html>
<?php $this->endPage() ?>