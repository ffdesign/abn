<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\HomeWidget;
use app\widgets\NewsScrollBlockWidget;
use app\widgets\MiniMenuWidget;
use app\helpers\YiiHelper; 
use app\helpers\TextHelper; 
use yii\caching\Cache;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
// AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <meta content="width=device-width, initial-scale=0.3,maximum-scale=3,user-scalable=1" name="viewport">
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Главная | АБН</title>
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
    <!-- <base href="http://www.abn.info/"></base> -->
    
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:700|PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="/styles/jquery.jscrollpane.css"/>
    <!--[if IE]><link rel="stylesheet" href="/styles/style_ie.css" type="text/css" media="screen, projection" /><![endif]--> 
    
    <script src="/js/jquery-1.10.2.min.js"></script> 
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.mousewheel.js"></script> 
    <script src="/js/modernizr-2.5.3.min.js"></script>
    <script src="/js/jquery.hc-sticky.js"></script>
    <script src="/js/script.js"></script>
    <link href="/images/fav.png" rel="shortcut icon" />
     
        
</head>

<body>
<?php $this->beginBody() ?>
    <section id="miniheader">
       <div class="miniheaderwrap">
         <table cellpadding="0" cellspacing="0" border="0" width="100%">
         <tr>
         <td align="left" valign="top">
            <div class="minilogo"><a href="/"><img src="/images/minilogo.png" title="АБН" border=0 ></a></div>
         </td>   
         <td align="left" valign="top">
            <nav>
                 <ul class="minimainmenu">
                    <?= MiniMenuWidget::widget(); ?>
                </ul>
            </nav>      
         </td>   
         <td align="left" valign="top"><a href="#" class="datepicker_link_mini"><span></span></a></td>   
         <td align="left" valign="top"><a href="#" class="searchform_link_mini"><span></span></a></td>
         </tr>
         </table>
         <div class="search_form_mini">
                        <form id="search_mini" action="/search" method="get" class="search">
                        <input type="text" name="search" value="" placeholder="Что ищем?">
                        <input type="submit" value="">
                        <a href="#" class="close_mini_search"></a>
                        </form>         
         </div>      
       </div>   
    </section>  

    
<div id="wrapper">
<div id="wrapperpage">

    <header id="header">
        
        <section class="today_area">
        <div class="today_block">
          <span class="day_name"><?= TextHelper::getDay(time()) ?></span> <span><?= TextHelper::formatDate(time(), 'm') ?></span>
          <span class="last_post_date">актуализация - <?= Yii::$app->getCache()->get('lastUpdate') ?> мск</span>
        </div>
        <div class="rss">
          <span class="rss"><a target="_blank" href="/rss"><span class="icon">RSS</span></a></span>
        </div>
        </section>
        
        <section class="cls">
           <div class="logo"><a href="/"><img src="/images/logo.png" title="АБН" border=0 ></a></div>
           <div class="submenu">
              <div class="cls">


                 <div class="area_search">
                        <form id="search" action="/search" method="get" class="search">
                        <input type="text" id='msearch' name="search" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>" placeholder="Поиск по сайту">
                        <input type="submit" value="">
                        </form>
                 </div>           
                 
                 <div class="area_celendar">
                  <a href="#" class="datepicker_link">Архив</a>
                 </div>
                 
                 <div class="area_curs">
                   <div class="area_curs_title">Официальные курсы:</div>
                   <div class="area_curs_block"><div style="clear: both"><?= Yii::$app->getCache()->get('lastCurrency') ?> </div></div>
                 </div>

                 
              </div>                
           </div>
        </section>

        <?= HomeWidget::widget(); ?>
        
        <section <?= (Yii::$app->request->getUrl() !== '/') ? 'id="content" class="content_index_topblock1"' : 'id="index_main_content_block"' ?>>
           <table cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td align="left" valign="top">
                <?=$content?>
            </td>   
            <td align="left" valign="top">
               <section class="partner_block <?= (Yii::$app->request->getUrl() !== '/') ? 'partner_block_nomargin' : '' ?>">
                    <? if(Yii::$app->request->getUrl() !== '/'): ?>
                        <a href="#"><img src="/images/240.jpg" border="0" width="240" height="400"></a>
                    <? endif; ?>
<!--                    <div class="part_block_title">Партнеры агентства</div>-->
<!--                    <div class="part_block_content">-->
<!--                            <div id="smi2adblock_79241"><a href="http://net.finam.ru/">Новости net.finam.ru</a></div>-->
<!--                                <script type="text/javascript" charset="utf-8">-->
<!--                                  (function() {-->
<!--                                    var sc = document.createElement('script'); sc.type = 'text/javascript'; sc.async = true;-->
<!--                                    sc.src = 'http://news.net.finam.ru/data/js/79241.js'; sc.charset = 'utf-8';-->
<!--                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sc, s);-->
<!--                                  }());-->
<!--                                </script>-->
<!--                            </div>-->
<!--                        </div>-->
                    
                    
               </section>
            </td>    
            </tr>
           </table>
        </section>
        
        

    </section><!-- #content-->
</div><!-- #wrapperpage -->
</div><!-- #wrapper -->

<footer id="footer">
    <nav>
        <ul class="footmenu">
            <li><a href="/pages/ob-agentstve" >Об агентстве</a></li>
            <li><a href="/forum">Форум</a></li> 
            <li><a href="/contacts" >Контакты</a></li>
            <li><a target="_blank" href="/rss" ><span class="icon">RSS-подписка</span></a></li> 
        </ul>
    </nav>
    <section class="footcontent">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
    <td align="left" valign="top"><div class="footlogo"><a href="/"><img src="/images/footlogo.png" title="АБН" border=0 ></a></div>
      <div class="cross-link">
        </div>
    </td>     
    <td align="left" valign="top">
    <div class="footcopy"><p><?=YiiHelper::getTextblock("Подвал: копирайт")?></p></div>
     <div class="foottext"><?=YiiHelper::getTextblock("Подвал: инфа")?></div>
      <div class="footbanners">
      <?=YiiHelper::getTextblock("Подвал: счетчики")?>
      </div>
    </td>
    </tr>
    </table>
    </section>
</footer><!-- #footer -->
<a class="scrollup" href="#"></a>
<div id="fade"></div>

    <script src="/js/picker.js"></script>
    <script src="/js/picker.date.js"></script>
    
    <link rel="stylesheet" href="/js/themes/classic.css" >
<link rel="stylesheet" href="/js/themes/classic.date.css" >
    
<script type="text/javascript">
$(document).ready(function(){
            
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    
    
 var shrinkHeader = 300;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('#miniheader').show();
        }
        else {
           $('#miniheader').hide();
        }
  });
function getCurrentScroll() {
    return window.pageYOffset;
    }   
    
    
    var rscroll = getCurrentScroll();
      if ( rscroll >= shrinkHeader ) {
           $('#miniheader').show();
        }
        else {
           $('#miniheader').hide();
        }   
    
    $('.datepicker_link, .datepicker_link_mini').pickadate({
        onSet: function() {
          var zref = this.get();
            if (zref=='') return false;
            //alert(zref);
          window.location = '/news/archive/'+zref;
        },
        onClose: function() { $('#fade').fadeOut(); } ,
        monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        weekdaysShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        firstDay: 1,
        clear: '',
        today: '',
        //highlight: '2014-05-05',
        //picker.set('highlight', [2014,5,20]),
        format: 'dd-mm-yyyy',
        max: true     
    });
    
    $('.datepicker_link_mini, .datepicker_link').click(function(){ $('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); return false });
    
        
    
     var picker = $('.datepicker_link, .datepicker_link_mini').pickadate('picker');
    //picker.set('select', [2013,3,20], { muted: true });
    
    $('.searchform_link_mini').click(function(){ 
      $('.search_form_mini').show();
      return false 
    }); 
    $('.close_mini_search').click(function(){ 
      $('.search_form_mini').hide();
      return false 
    });
    
    
    
    $('.lenta_block_scroll').css("height", (($(window).height())-160)  + "px"); 
    $(window).resize(function() {
        $('.lenta_block_scroll').css("height", (($(window).height())-160)  + "px"); 
    });
    
    $('.lenta_block_scroll').jScrollPane();
    $('.lenta_block').hcSticky({top: 40});
});
</script>

<!-- <?php $this->endBody() ?> -->
</body>
</html>
<?php $this->endPage() ?>