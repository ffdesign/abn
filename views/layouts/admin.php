<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\NewsCategories;
use app\helpers\YiiHelper;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    <link href="/css/admin.css" rel="stylesheet">

    <link href="/images/fav.png" rel="shortcut icon" />
</head>
<body>
<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'На сайт',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    !YiiHelper::is_auth() ?
                        ['label' => 'Авторизация', 'url' => ['/users/login']] :
                        ['label' => 'Выход (' . YiiHelper::getAuthName() . ')',
                            'url' => ['/users/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 col-md-2 sidebar">
            <?php
                $news_categories = array();
                $news_categories[] = "<li class='dropdown-header'>Управление новостями</li>";
                $news_categories[] = array('label'=>'Управление категориями', 'url' => '/news-categories/index');
                $news_categories[] = array('label'=>'Все новости', 'url' => '/news/index');
                $news_categories[] = "<br><li class='dropdown-header'>Категории</li>";
                foreach (NewsCategories::find()->where('is_visible=1')->all() as $category):
                    $news_categories[] = array('label'=>$category->title, 'url' => '/news/index/'.$category->id);
                endforeach;
             ?>
            <?php echo Nav::widget([
                'options' => ['class'=>'nav nav-sidebar'],
                'items' => [
                    ['label' => 'Панель новостей', 'url' => ['/news/index']],
                    ['label' => 'Текстовые страницы', 'url' => ['/pages/index']],
                    ['label' => 'Текстовые блоки', 'url' => ['/textblocks/index']],
                    ['label' => 'Баннеры', 'url' => ['/banners/index']],
                    ['label' => 'Новости',
                        'items' => $news_categories,
                    ],
                    ['label' => 'Рассылка', 'url' => ['/subscribe-data/index']],
                    ['label' => 'Редакция', 'url' => ['/users/index']],
                ],
            ]);
            ?>
          </div>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Панель администратора</h1>
              <?= Breadcrumbs::widget([
                  'homeLink' => false,
                  // 'homeLink' => ['label' => 'Панель новостей', 'url' => '/news/index'],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              ]) ?>
              <?= $content ?>
          </div>
        </div>
      </div>
 
    </div>

   
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>