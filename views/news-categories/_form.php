<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\NewsCategories $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <? if(!$model->isNewRecord): ?>
            <?= $form->field($model, 'path')->textInput(['maxlength' => 255]) ?>
    <? endif; ?>

   <?= $form->field($model, 'color')->dropDownList(
           array(
            ''=>'-',
            '#306cc7'=>'Синий',
            '#8e44ad'=>'Фиолетовый',
            '#00a3cb'=>'Голубой',
            '#009a4d'=>'Зеленый',
            '#c12945'=>'Красный',
            '#f26522'=>'Оранжевый',
            '#b0a62e'=>'Коричневый',
            '#6b7b84'=>'Серый',
            )
        );
    ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'is_home')->checkbox() ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
