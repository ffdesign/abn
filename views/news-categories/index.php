<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NewsCategoriesSearch $searchModel
 */

$this->title = 'Новости: категории';
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->session->hasFlash('flashMessage')):
    echo Alert::widget([
    'options' => [
        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
        ],
        'body' => Yii::$app->session->getFlash('flashMessage')[1],
    ]);
endif;
?>
<div class="news-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'Заголовок',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'Цвет',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return "<div width='10px' height='10px' style='background: ".$model->color."'>&nbsp;</div>";
                },
            ],
            [
                'attribute' => 'Видимость',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->is_visible ? 'Да' : 'Нет';
                },
            ],
            [
                'attribute' => 'Показывать на главной',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->is_home ? 'Да' : 'Нет';
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '/news-categories/'.$model->path);
                    }, 
                ],
            ],
        ],
    ]); ?>

</div>
