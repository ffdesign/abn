<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NewsCategories $model
 */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Новости: категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
