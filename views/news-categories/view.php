<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\TextHelper; 
use app\helpers\YiiHelper; 

/**
 * @var yii\web\View $this
 * @var app\models\NewsCategories $model
 */

$this->title = $newsCategories->title. ' | АБН';
?>

<section class="lead_block">
    <section class="razdel_block">
        <div class="razdel_caption" style="background-color: <?=$newsCategories->color?>"><?=$newsCategories->title?></div>
        
        <?// if ($this->beginCache('2', ['duration' => 300])): ?>
            <? foreach ($model as $news): ?>
                <div class="razdel_block_news_item cls <?= $news->is_hot ? 'hotitem' : '' ?>">
                    <? if($news->oneImage): ?>
                        <? $image = '/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-115x76.png'; ?>
                        <a href="<?='/news/'.$newsCategories->path.'/'.$news->path?>" class="razdel_block_news_item_img"><img src="<?=$image?>" border=0 class="raisedsm"></a>
                    <? endif; ?>

                    <div class="razdel_block_news_item_linkblock <?= !$news->oneImage ? 'fullwidth' : '' ?> ">
                        <div class="post_data"><span><?= TextHelper::formatDate($news->created, 'mt') ?></span></div>
                        <a href="<?='/news/'.$newsCategories->path.'/'.$news->path?>" class="razdel_block_news_item_link"><?=$news->title?></a>                            
                        <div class="razdel_block_news_item_text"><?=$news->preview?></div>
                    </div>
                </div>
            <? endforeach; ?>
        <?// $this->endCache(); endif;?>


        <? if($model && $count > $pageSize): ?>
            <div class="loadNews"></div>
            <div class="razdel_more_link" ><a href="#" id='loadNews' alt="2" data-categoryId="<?=$newsCategories->id?>"><span>Показать больше новостей</span></a></div>
        <? endif; ?>
        <? $banner = YiiHelper::getBanners($newsCategories->title);  ?>
        <? if($banner): ?>
            <div class="razdel_banner_468"><a target="_blank" href="<?=$banner['url']?>"><img width="465" height="60" src="/images/banners/<?=$banner['image']?>"></a></div>
        <? endif; ?>
    </section>
</section>