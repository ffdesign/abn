<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Subscribe $model
 */

$this->title = 'Подписка на рассылку | АБН';
?>

<section class="article_block">
    <h1>Подписка на рассылку </h1>
    <? if(isset($message)): ?>
		<?= $message; ?>
	<? endif; ?>
		<p style="font-size: 16px; font-family: PT Sans; line-height: 26px; font-weight: bold; color: #606060">Подпишитесь на нашу ежедневную рассылку по итогам дня — держите руку на пульсе главных новостей кредитно-финансовой сферы.</p>
	
		<section class="article_block_content">
	        <?= $this->render('_form_subscribe', [
		        'model' => $model,
		    ]) ?>
	    </section>
		
	    <img style="margin-top: 20px; margin-bottom: 20px;" src="/images/subscribe-example.png">
</section>