<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Subscribe $model
 */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Рассылка', 'url' => ['/subscribe-data/index']];
$this->params['breadcrumbs'][] = ['label' => 'Подписчики', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
