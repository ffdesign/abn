<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Subscribe $model
 */

$this->title = 'Отписаться от рассылки | АБН';
?>

<section class="article_block">
    <h1>Отписаться от рассылки</h1>
    <? if(!empty($message)): ?>
		<p style="font-size: 14px;"><?= $message; ?>.</p>
	<? else: ?>
		<section class="article_block_content">
	        <?= $this->render('_form_subscribe', [
		        'model' => $model,
		    ]) ?>
	    </section>
	<? endif; ?>
    
</section>

