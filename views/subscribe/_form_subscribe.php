<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Subscribe $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="subscribe-form">

    <?php $form = ActiveForm::begin(); ?>
	<table>
		<tr valign="top">
			<td>
				<?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'class' => 'sub', 'placeholder' => 'Ваш email'])->label('') ?>
			</td>
			<td>
				<?= Html::submitButton('ОК', ['class' => $model->isNewRecord ? 'btnsub btn-success' : 'btnsub btn-primary']) ?>
			</td>
		</tr>
	</table>
   
	

    <?php ActiveForm::end(); ?>

</div>