<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Textblocks $model
 */

$this->title = 'Create Textblocks';
$this->params['breadcrumbs'][] = ['label' => 'Textblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textblocks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
