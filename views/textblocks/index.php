<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\TextblocksSearch $searchModel
 */

$this->title = 'Текстовые блоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textblocks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute' => 'Заголовок',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'Видимость',
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->is_visible ? 'Да' : 'Нет';
                },
            ],
        ],
    ]); ?>

</div>
