<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\models\NewsCategories;
use app\models\search\NewsCategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\filters\VerbFilter;
use app\helpers\TextHelper;
use app\helpers\YiiHelper;
use yii\data\Pagination;
use yii\db\Connection;
/**
 * NewsCategoriesController implements the CRUD actions for NewsCategories model.
 */
class NewsCategoriesController extends Controller
{
    public $enableCsrfValidation = false; //Отключаем csfr валидацию, иначе не будет работать загрузка изображений и файлов
    public $pageSize = 10;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $searchModel = new NewsCategoriesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single NewsCategories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($category)
    {
        $connection = new Connection;

            $newsCategories = NewsCategories::find()->where("path = '$category'")->one();
            
            if(!$newsCategories)
                throw new NotFoundHttpException('The requested page does not exist.');

            $query = News::find()->where(['is_visible' => 1, 'id_category' => $newsCategories->id])->orderBy("created desc");

        return $this->render('view', [
            'model' => $query->limit($this->pageSize)->all(),
            'count' => $query->count(),
            'pageSize' => $this->pageSize,
            'newsCategories' => $newsCategories,
        ]);
    }

    public function actionPage($category)
    {
        if(Yii::$app->getRequest()->getIsAjax()):
            $query = News::find()->where(['is_visible' => 1, 'id_category' => $category])->orderBy("created desc");
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);
            $pages->setPageSize($this->pageSize);
            $models = $query->offset($pages->offset)->limit($pages->limit)->all();
            $max_pages = $countQuery->count() / $this->pageSize;
            if($_POST['current_page'] > $max_pages) {
                return false;
            } else {
                return $this->renderPartial('_page', [
                    'model' => $models,
                    'pages' => $pages,
                ]);
            }
        endif;
    }

    /**
     * Creates a new NewsCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = new NewsCategories;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->path = TextHelper::translite(Yii::$app->request->post()['NewsCategories']['title']);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая категория добавлена.'));
                Yii::$app->getCache()->set('updateNews', true);
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NewsCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Категория "'.$model->title.'" успешно изменена.'));
            Yii::$app->getCache()->set('updateNews', true);
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            $this->layout = 'admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NewsCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');
        
        $model = $this->findModel($id);
        Yii::$app->session->setFlash('flashMessage', array('success', 'Категория "'.$model->title.'" удалена.'));
        Yii::$app->getCache()->set('updateNews', true);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NewsCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
