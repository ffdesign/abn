<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\search\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;
use app\helpers\YiiHelper;
/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin() 
    {   
        $model = new Users;
        $session = new Session;
        if (Yii::$app->request->post()) {
            $email = Yii::$app->request->post()['Users']['email'];
            $password = md5(Yii::$app->request->post()['Users']['password']);
            $check = $model->find()->where("email = '$email' && password = '$password'")->one();
            if($check) {
                $session->set("auth", md5($email));
                $this->redirect('/news/index');
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        $session = new Session;
        $session->remove('auth');
        $this->redirect('/users/login');
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $searchModel = new UsersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = new Users;
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->password = md5(Yii::$app->request->post()['Users']['password']);
            $model->token = md5(Yii::$app->request->post()['Users']['email']);
            if ($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новый редактор добавлен.'));
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->password = md5(Yii::$app->request->post()['Users']['password']);
            $model->token = md5(Yii::$app->request->post()['Users']['email']);
            if ($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Редактор "'.$model->email.'" успешно изменен.'));
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        Yii::$app->session->setFlash('flashMessage', array('success', 'Редактор "'.$model->email.'" удален.'));
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
