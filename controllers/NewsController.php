<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\models\NewsCategories;
use app\models\Images;
use app\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\TextHelper;
use app\helpers\YiiHelper;
use yii\web\UploadedFile;
use yii\web\Request;
use yii\imagine\Image;
use yii\data\Pagination;
/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public $enableCsrfValidation = false; //Отключаем csfr валидацию, иначе не будет работать загрузка изображений и файлов
    public $pageSize = 10;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $searchModel = new NewsSearch;

        if(isset($_GET['id'])) {
            $categoryInfo = NewsCategories::findOne($_GET['id']);
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
             $categories = NewsCategories::find()->where(['is_visible' => 1])->all();
            $this->layout = 'admin';
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'categoryInfo' => $categoryInfo,
                'categories' => $categories,
            ]);
        } else {
            $categories = NewsCategories::find()->where(['is_visible' => 1])->all();
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
            $this->layout = 'admin';
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'categories' => $categories,
            ]);
        }
        
    }

    public function actionAll()
    {
        $query = News::find()->where(['is_visible' => 1])->orderBy("created desc");
        $this->layout = 'twocolumn';
        return $this->render('all', [
                'model' => $query->limit($this->pageSize)->all(),
                'count' => $query->count(),
                'pageSize' => $this->pageSize,
            ]);
    }

    public function actionPage()
    {
        if(Yii::$app->getRequest()->getIsAjax()):
            $query = News::find()->where(['is_visible' => 1])->orderBy("created desc");
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);
            $pages->setPageSize($this->pageSize);
            $models = $query->offset($pages->offset)->limit($pages->limit)->all();
            $max_pages = $countQuery->count() / $this->pageSize;
            if($_POST['current_page'] > $max_pages) {
                return false;
            } else {
                $this->layout = 'no-layout';
                return $this->renderPartial('_page', [
                    'model' => $models,
                    'pages' => $pages,
                ]);
            }
        endif;
    }

    public function actionArchive($date)
    {
        $model = NewsCategories::find()->where(['is_visible' => 1])->all();

        return $this->render('archive', [
                'model' => $model,
                'date' => $date
            ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id=null, $path=null)
    {   
        $model = $this->findModel($id, $path);
        
        if(!empty($model->postponed) || $model->is_visible != 1) 
            throw new NotFoundHttpException();

        $otherNews = News::find()->where(['is_visible' => 1, 'id_category' => $model->category->id])->andWhere(['not like', 'id', $model->id])->limit(4)->orderBy("created desc")->all();
        $linkNews = News::find()->where(['is_visible' => 1])->andWhere(['not like', 'id', $model->id])->andWhere(['like', 'tags', $model->tags])->limit(3)->orderBy("created desc")->all();
        
        $this->layout = 'news';

        return $this->render('view', [
            'model' => $model,
            'otherNews' => $otherNews,
            'linkNews' => $linkNews,
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $id = $_GET['id'];
        $model = new News;
        if(isset($id))
            $model->id_category = $id;
        $categoryInfo = NewsCategories::findOne($id);

        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->id_user = YiiHelper::getAuthId();

            if(!empty(Yii::$app->request->post()['News']['created']))
                $model->created = strtotime(Yii::$app->request->post()['News']['created']." ".date("h:i:s"));
            else
                $model->created = time();

            if(!empty(Yii::$app->request->post()['News']['postponed'])) {
                $model->postponed = strtotime(Yii::$app->request->post()['News']['postponed']);
                $model->is_visible = 0;
            }

            $model->path = $id . date('ds', time()) .'-'. TextHelper::translite(Yii::$app->request->post()['News']['title']);

            if($model->save()) {
                $this->uploadImage($model->id);
                Yii::$app->getCache()->set('updateNews', true);
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая новость добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
                'categoryInfo' => $categoryInfo,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        $currentDate = $model->created;
        $model->created = date('d.m.Y', $model->created);
        if(!empty($model->postponed))
            $model->postponed = date('d.m.Y H:i', $model->postponed);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            $model->created = $currentDate;

            if(Yii::$app->request->post()['News']['updateDate'] == 1) {
                if(!empty(Yii::$app->request->post()['News']['created']))
                    $model->created = strtotime(Yii::$app->request->post()['News']['created']." ".date("H:i"));
                else
                    $model->created = time();
            }

            if(!empty(Yii::$app->request->post()['News']['postponed'])) {
                $model->postponed = strtotime(Yii::$app->request->post()['News']['postponed']);
                $model->is_visible = 0;
            }
            
            $model->updated = time();

            if($model->save()) {
                $this->uploadImage($id);
                Yii::$app->getCache()->set('updateNews', true);
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новость "'.$model->title.'" успешно изменена.'));
                return $this->redirect(['index']);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function uploadImage($id)
    {
        if(isset($_FILES['News'])) {
            $dirUpload = 'images/news/';
            if(!file_exists($dirUpload.$id)) {
                mkdir($dirUpload.$id);
                mkdir($dirUpload.$id.'/thumb');
            }
            $i = -1;
            foreach ($_FILES['News']['name']['image'] as $image) {
                $i++;
                $tmpName = $_FILES['News']['tmp_name']['image'][$i];
                $ext = pathinfo($image, PATHINFO_EXTENSION);
                $imageName = md5(date($image.date('dmYhis'))).'.'.$ext;
                if(move_uploaded_file($tmpName, $dirUpload.$id.'/'.$imageName)) {
                    $model = new Images;
                    $model->id_news = $id;
                    $model->image = $imageName;
                    $model->author = Yii::$app->request->post()['News']['image_authorc'][$i];
                    $model->text = Yii::$app->request->post()['News']['image_previewc'][$i];
                    $model->save();

                    //preview
                    $file = 'images/news/'.$id.'/thumb/'.md5($imageName.$id);
                    Image::thumbnail('images/news/'.$id.'/'.$imageName, 200, 200)->save($file.'.png', ['quality' => 1]); 
                    Image::thumbnail('images/news/'.$id.'/'.$imageName, 115, 76)->save($file.'-115x76.png', ['quality' => 1]); 
                    Image::thumbnail('images/news/'.$id.'/'.$imageName, 306, 180)->save($file.'-306x180.png', ['quality' => 1]); 
                    Image::thumbnail('images/news/'.$id.'/'.$imageName, 467, 257)->save($file.'-467x257.png', ['quality' => 1]); 
                }
            }
        }

        if(Yii::$app->request->post()['News']['image_new'] !== 1 && isset(Yii::$app->request->post()['News']['image_author'])) {
            $model = new Images;
            foreach (Yii::$app->request->post()['News']['image_author'] as $key => $value) {
                $update = $model->find()->where(['id'=>$key])->one();
                $update->author = Yii::$app->request->post()['News']['image_author'][$key];
                $update->text = Yii::$app->request->post()['News']['image_preview'][$key];
                $update->save();
            }
        }
    }

    public function actionDeleteImage($id) 
    {
        $model = Images::find()->where(['id'=>$id])->one();
        $file = 'images/news/'.$model->id_news.'/'.$model->image;
        if(file_exists($file))
            unlink($file);
        $model->delete();
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');
        
        $model = $this->findModel($id);
        $id_category = $model->category->id;
        Yii::$app->getCache()->set('updateNews', true);
        Yii::$app->session->setFlash('flashMessage', array('success', 'Новость "'.$model->title.'" удалена.'));
        $model->delete();

        return $this->redirect(['/news/index/'.$id_category]);
    }

    public function actionOnVisible($id) 
    {
        $model = $this->findModel($id);
        $model->is_visible = 1;
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return 'Новость "'.$model->title.'" <b>включена</b> для отображения';
    }

    public function actionOffVisible($id) 
    {
        $model = $this->findModel($id);
        $model->is_visible = 0;
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return 'Новость "'.$model->title.'" <b>выключена</b> для отображения';
    }

    public function actionAddTop($id, $top) 
    {
        $model = $this->findModel($id);
        $model->is_top = $top;
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return 'Новость "'.$model->title.'" <b>добавлена</b> в ТОП'.$top;
    }

    public function actionRemoveTop($id)
    {
        $model = $this->findModel($id);
        $model->is_top = '';
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return 'Новость "'.$model->title.'" <b>удалена</b> из ТОПа';
    }

    public function actionAddHot($id) 
    {
        $model = $this->findModel($id);
        $model->is_hot = 1;
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return  'Новость "'.$model->title.'" <b>добавлена</b> в горячее';
    }

    public function actionRemoveHot($id)
    {
        $model = $this->findModel($id);
        $model->is_hot = 0;
        Yii::$app->getCache()->set('updateNews', true);
        if($model->save())
            return  'Новость "'.$model->title.'" <b>удалена</b> из горячего';
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id=null, $path=null)
    {
        if ($id) {
            $model = News::find()->where(['id'=>$id])->one();
        } elseif ($path) {
            $model = News::find()->where(['path'=>$path])->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
