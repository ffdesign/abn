<?php

namespace app\controllers;

use Yii;
use app\models\Pages;
use app\models\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\TextHelper;
use app\helpers\YiiHelper;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $searchModel = new PagesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id=null, $path=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $path),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = new Pages;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->path = TextHelper::translite(Yii::$app->request->post()['Pages']['title']);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая текстовая страница добавлена.'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('flashMessage', array('success', 'Текстовая страница "'.$model->title.'" успешно изменена.'));
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            $this->layout = 'admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        Yii::$app->session->setFlash('flashMessage', array('success', 'Текстовая страница "'.$model->title.'" удалена.'));
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id=null, $path=null)
    {
        if ($id) {
            $model = Pages::find()->where(['id'=>$id])->one();
        } elseif ($path) {
            $model = Pages::find()->where(['path'=>$path])->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
