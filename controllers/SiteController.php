<?php

namespace app\controllers;

use Yii;
use DateTime;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\News;
use app\models\NewsCategories;
use app\helpers\YiiHelper; 
use app\helpers\TextHelper; 
use yii\db\Connection;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\StringHelper;
class SiteController extends Controller
{
    public $enableCsrfValidation = false; //Отключаем csfr валидацию, иначе не будет работать загрузка изображений и файлов
    public $pageSize = 10;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $model = NewsCategories::find()->where("is_visible = 1")->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionSearch() 
    {
        $this->layout = 'twocolumn';

        if(Yii::$app->request->get()) {
            $search = Yii::$app->request->get()['search'];
            $explode = explode(' ', $search);
            $query = News::find()->where("is_visible = 1")->orderBy('created desc');

            foreach($explode as $search_row) {
                if(strlen(utf8_decode($search_row)) > 5)
                    $search_row = mb_substr($search_row, 0, 10);

                $search_row = TextHelper::strip($search_row);
                if(isset(Yii::$app->request->get()['ontitle']))
                    $query->andWhere("title LIKE '%$search_row%'");
                else
                    $query->andWhere("title LIKE '%$search_row%' OR text LIKE '%$search_row%' OR preview LIKE '%$search_row%'");
            }
            $sortDate = '';
            $sortRelevance = '';
            if(isset(Yii::$app->request->get()['date']))
                $sortDate = Yii::$app->request->get()['date'];

            if(isset(Yii::$app->request->get()['relevance']))
                $sortRelevance = Yii::$app->request->get()['relevance'];

            if($sortDate == 'asc' || $sortDate == 'desc')
                $query = $query->orderBy("created ". $sortDate);

            if($sortRelevance == 'asc' || $sortRelevance == 'desc')
                $query = $query->orderBy("text ". $sortRelevance);


            $arCategory = array();
            $i = 0;
            foreach ($query->all() as $category): 
                $i++;
                $arCategory[$i] = $category->category->title;
            endforeach;

            $listCategory = NewsCategories::find()->select('id, title, color')->where(['is_visible' => 1])->all();

            return $this->render('search', [
                    'model' => $query->limit($this->pageSize)->all(),
                    'count' => $query->count(),
                    'pageSize' => $this->pageSize,
                    'sortDate' => $sortDate,
                    'sortRelevance' => $sortRelevance,
                    'arCategory' => array_unique($arCategory),
                    'listCategory' => $listCategory
                ]);
        } else
            return $this->render('search');
    }

    public function actionPage($search)
    {
        if(Yii::$app->getRequest()->getIsAjax()):
            $explode = explode(' ', $search);
            $query = News::find()->where("is_visible = 1")->orderBy('created desc');

            foreach($explode as $search_row) {
                if(strlen(utf8_decode($search_row)) > 5)
                    $search_row = mb_substr($search_row, 0, 10);

                $search_row = TextHelper::strip($search_row);
                if(isset(Yii::$app->request->get()['ontitle']))
                    $query->andWhere("title LIKE '%$search_row%'");
                else
                    $query->andWhere("title LIKE '%$search_row%' OR text LIKE '%$search_row%' OR preview LIKE '%$search_row%'");
            }
            $sortDate = '';
            $sortRelevance = '';
            if(isset(Yii::$app->request->get()['date']))
                $sortDate = Yii::$app->request->get()['date'];

            if(isset(Yii::$app->request->get()['relevance']))
                $sortRelevance = Yii::$app->request->get()['relevance'];

            if($sortDate == 'asc' || $sortDate == 'desc')
                $query = $query->orderBy("created ". $sortDate);

            if($sortRelevance == 'asc' || $sortRelevance == 'desc')
                $query = $query->orderBy("text ". $sortRelevance);

            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);
            $pages->setPageSize($this->pageSize);
            $models = $query->offset($pages->offset)->limit($pages->limit)->all();
            $max_pages = $countQuery->count() / $this->pageSize;
            if($_POST['current_page'] > $max_pages) {
                return false;
            } else {
                return $this->renderPartial('_page', [
                    'model' => $models,
                    'pages' => $pages,
                ]);
            }
        endif;
    }

    public function actionUploadImage()
    {
        $dir = 'images/upload/';
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
        || $_FILES['file']['type'] == 'image/jpg'
        || $_FILES['file']['type'] == 'image/gif'
        || $_FILES['file']['type'] == 'image/jpeg'
        || $_FILES['file']['type'] == 'image/pjpeg')
        {
            $file = md5(date('YmdHis')).'.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            move_uploaded_file($_FILES['file']['tmp_name'], $dir.$file);
            $array = array(
                'filelink' => '/images/upload/'.$file
            );
            echo stripslashes(json_encode($array));
        }
    }

    public function actionUploadFile()
    {
        $dir = 'doc/';
        $file = md5(date('YmdHis')).'.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $dir.$file))
        {
             $array = array(
                'filelink' => '/doc/'.$file,
                'filename' => $_FILES['file']['name']
             );
             echo stripslashes(json_encode($array));
        }
    }

    public function actionError() 
    {
        return $this->render('error');
    }

    public function actionRss()
    {
        $model = News::find()->where(['is_visible' => 1])->limit(20)->orderBy('created DESC')->all();
        $lastThreeNews = News::find()->where(['is_visible' => 1])->limit(3)->orderBy('created DESC')->all();
        $feed = new \Zelenin\Feed;

        $feed->addChannel(Url::toRoute('/', true));
        $feed
            ->addChannelTitle('Агенство банковских новостей')
            ->addChannelLink(Url::toRoute('/', true))
            ->addChannelDescription('Агентство банковских новостей - независимый российский информационный сервис для профессионалов кредитно-финансовой сферы и широкого круга потребителей.')
            ->addChannelLanguage(Yii::$app->language)
            ->addChannelElement('yandex:logo', 'http://abn.info/images/abn.png')
            ->addChannelElement('yandex:logo', 'http://abn.info/images/abn_square.png', array('type' => 'square'));
            foreach ($model as $news) {
            $feed->addItem();
            $feed
                ->addItemGuid('http://abn.info/news/'.$news->category->path.'/'.$news->path)
                ->addItemTitle($news->title)
                ->addItemLink('http://abn.info/news/'.$news->category->path.'/'.$news->path)
                ->addItemElement('pdalink', 'http://m.abn.info/news/'.$news->category->path.'/'.$news->path)
                ->addItemDescription($news->preview)
                // ->addItemAuthor('АБН')
                ->addItemCategory($news->category->title);
            if(isset($news->oneImage->image))
                $feed->addItemEnclosure('http://abn.info/images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-467x257.png', 189, 'image/jpeg');
        
            $feed
                ->addItemPubDate(date('Y-m-d H:i:s' , $news->created))
                ->addItemElement('yandex:full-text', preg_replace('#(<strong>(.*?)<\/strong>)#is', '', $news->text));
        }
  
        echo $feed;
    }

    public function actionRssRambler()
    {   
        $model = News::find()->where(['is_visible' => 1])->limit(20)->orderBy('created DESC')->all();
        $lastThreeNews = News::find()->where(['is_visible' => 1])->limit(3)->orderBy('created DESC')->all();
        $feed = new \Zelenin\Feed;

        $feed->addChannel(Url::toRoute('/', true));
        $feed
            ->addChannelTitle('Агенство банковских новостей')
            ->addChannelLink(Url::toRoute('/', true))
            ->addChannelDescription('Агентство банковских новостей - независимый российский информационный сервис для профессионалов кредитно-финансовой сферы и широкого круга потребителей.')
            ->addChannelLanguage(Yii::$app->language);

        foreach ($model as $news) {
            $feed->addItem();
            $feed
                ->addItemGuid('http://abn.info/news/'.$news->category->path.'/'.$news->path)
                ->addItemTitle($news->title)
                ->addItemLink('http://abn.info/news/'.$news->category->path.'/'.$news->path)
                ->addItemPubDate(date('Y-m-d H:i:s' , $news->created))
                ->addItemDescription($news->preview)
                ->addItemCategory($news->category->title)
                ->addItemElement('rambler:fulltext', '<![CDATA['.$news->text.']]>')
                ->addItemAuthor('АБН');
            if(isset($news->oneImage->image))
                $feed->addItemEnclosure('images/news/'.$news->id.'/thumb/'.md5($news->oneImage->image.$news->id).'-467x257.png', 189, 'image/jpeg');
        }

        foreach ($lastThreeNews as $threeNews) {
            $feed->addItemElement('rambler:related', '', array(
                'url' => 'http://abn.info/news/'.$threeNews->category->path.'/'.$threeNews->path,
                'title' => $threeNews->title, 
                'rel' => 'news', 
                'type' => 'text/html', 
                'pubdate' => $this->normalizeDateTime($threeNews->created), 
                'author' => 'АБН')
            );
        }
        


        echo $feed;
    }

    private function normalizeDateTime($value)
    {
        if ($value instanceof DateTime) {
            $datetime = $value;
        } else {
            if (is_numeric($value)) {
                $datetime = new DateTime();
                $datetime->setTimestamp($value);
            } else {
                try {
                    $datetime = new DateTime($value);
                } catch (Exception $e) {
                    echo $e->getMessage();
                    exit(1);
                }
            }
        }
        return $datetime->format(DATE_RSS);
    }
}
