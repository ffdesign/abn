<?php

namespace app\controllers;

use Yii;
use app\models\SubscribeData;
use app\models\News;
use app\models\Subscribe;
use app\models\search\SubscribeDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\TextHelper;

/**
 * SubscribeDataController implements the CRUD actions for SubscribeData model.
 */
class SubscribeDataController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubscribeData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubscribeDataSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSubscribers()
    {
        $model = new Subscribe;
        
        $this->layout = 'admin';
        return $this->render('subscribers', [
            'model' => $model->find()->all(),
            ]);
    }

    /**
     * Creates a new SubscribeData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubscribeData;
        $news = new News;
        $subscribe = new Subscribe;
        $newsId = array();
        $current_date = date('d.m.Y', strtotime('now'));
        $yesterday_date = date('d.m.Y', strtotime('-1 day'));
        $getNews = $news->find()->where("FROM_UNIXTIME(created, '%d.%m.%Y') = '$current_date' OR FROM_UNIXTIME(created, '%d.%m.%Y') = '$yesterday_date' AND is_visible = '1'")->all();
        
        $sendEmail = $subscribe->find();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $sendNewsOne = false;

            if(isset($_POST['SubscribeData']['news'])) {
                foreach ($_POST['SubscribeData']['news'] as $key => $value) 
                    $newsId[] = $key;
            }

            //главная новость
            if(isset($_POST['SubscribeData']['news_index'])) {
                $sendNewsOne = $news->find()->where(['id' => $_POST['SubscribeData']['news_index']])->one();
            }

            $sendNews = $news->find()->where(['id' => array_unique($newsId)]);

            $mailInfo = array();
            $mailInfo['title'] = $_POST['SubscribeData']['title'];
            $mailInfo['day'] = TextHelper::getDay(time());
            $mailInfo['date'] = TextHelper::formatDate(time(), 'mt');
            $mailInfo['countNews'] = $sendNews->count();
            $message = $this->renderPartial('mail', [
                'sendNews' => $sendNews->all(),
                'sendNewsOne' => $sendNewsOne,
                'mailInfo' => $mailInfo,
            ]);

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'From: АБН <noreply@abn.info>' . "\r\n";

            if(isset($_POST['SubscribeData']['emails'])) {
                $emails = $_POST['SubscribeData']['emails'];
                $count = 0;
                foreach ($emails as $email) {
                    $count++;
                    mail($email, $_POST['SubscribeData']['title'], $message, $headers);
                }
                $model->count_sends = $count;
            } else {
                $emails = $sendEmail->all();
                foreach ($emails as $email) {
                    mail($email->email, $_POST['SubscribeData']['title'], $message, $headers);
                }
                $model->count_sends = $sendEmail->count();
            }

            $model->count_news = $sendNews->count();
            
            if($model->save()){
                return $this->redirect(['index', 'id' => $model->id]);
            }
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
                'news' => $getNews,
                'emails' => $sendEmail->all()
            ]);
        }
    }

    /**
     * Deletes an existing SubscribeData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubscribeData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubscribeData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubscribeData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
