<?php

namespace app\controllers;

use Yii;
use app\models\Subscribe;
use app\models\search\SubscribeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubscribeController implements the CRUD actions for Subscribe model.
 */
class SubscribeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Subscribe;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('create_subscribe', [
                'model' => $model,
                'message' => '<div id="modal-background" class="active"></div>
                <div id="modal-content" class="active">
                    <span id="modal-text">Вы успешно подписаны<br> на новостную рассылку</span>
                </div>',
            ]);
        } else {
            return $this->render('create_subscribe', [
                'model' => $model,
            ]);
        }
    }

    public function actionUnsubscribe() 
    {
        $model = new Subscribe;
        $message = '';
        if (Yii::$app->request->post()) {
            $find = $model->find()->where(['email' => Yii::$app->request->post()['Subscribe']['email']])->one();
            if($find) {
                $find->delete();
                
                $message = 'Вы успешно отписались от рассылки';
            }
        }

        return $this->render('unsubscribe', [
                'model' => $model,
                'message' => $message
            ]);
    }

    public function actionAdmin() 
    {
        $searchModel = new SubscribeSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Subscribe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscribe;
        $this->layout = 'admin';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Subscribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->layout = 'admin';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Subscribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Subscribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscribe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
