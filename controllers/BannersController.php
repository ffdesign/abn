<?php

namespace app\controllers;

use Yii;
use app\models\Banners;
use app\models\NewsCategories;
use app\models\search\BannersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\YiiHelper;
use yii\imagine\Image;

/**
 * BannersController implements the CRUD actions for Banners model.
 */
class BannersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $searchModel = new BannersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $this->layout = 'admin';
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = new Banners;
        $newsCategory = NewsCategories::find()->where('is_visible=1')->all();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post()); 

            if($_FILES['Banners']) {
                $this->uploadImage($model);
            }

            
            $model->place = Yii::$app->request->post()['Banners']['place'];
            if($model->save())
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новый баннер успешно добавлен.'));
            Yii::$app->getCache()->set('updateNews', true);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $this->layout = 'admin';
            return $this->render('create', [
                'model' => $model,
                'newsCategory' => $newsCategory,
            ]);
        }
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        $newsCategory = NewsCategories::find()->where('is_visible=1')->all();

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($_FILES['Banners']['name']['_image'])) {
                $this->uploadImage($model);
            }
            $model->place = Yii::$app->request->post()['Banners']['place'];
            Yii::$app->getCache()->set('updateNews', true);
            if($model->save())
                Yii::$app->session->setFlash('flashMessage', array('success', 'Баннер "'.$model->title.'" успешно изменен.'));
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            $this->layout = 'admin';
            return $this->render('update', [
                'model' => $model,
                'newsCategory' => $newsCategory,
            ]);
        }
    }

    public function uploadImage($model)
    {
        $dir = 'images/banners/';
        $ext = pathinfo($_FILES['Banners']['name']['_image'], PATHINFO_EXTENSION);
        $imageName = md5($_FILES['Banners']['name']['_image'].date('dmYhis')).'.'.$ext;
        $model->image = $imageName;
        move_uploaded_file($_FILES['Banners']['tmp_name']['_image'], $dir.$imageName);
        Image::thumbnail('images/banners/'.$imageName, 200, 200)->save($dir.'thumb-'.$imageName, ['quality' => 100]); 
        Image::thumbnail('images/banners/'.$imageName, 240, 400)->save($dir.'240x400-'.$imageName, ['quality' => 100]); 
        Image::thumbnail('images/banners/'.$imageName, 1000, 90)->save($dir.'1000x90-'.$imageName, ['quality' => 100]); 
        Image::thumbnail('images/banners/'.$imageName, 465, 60)->save($dir.'465x60-'.$imageName, ['quality' => 100]); 
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        if(!YiiHelper::is_auth())
            $this->redirect('/users/login');

        $model = $this->findModel($id);
        Yii::$app->session->setFlash('flashMessage', array('success', 'Баннер "'.$model->title.'" удален.'));
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
